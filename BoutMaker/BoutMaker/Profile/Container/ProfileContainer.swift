//
//  ProfileContainer.swift
//  BoutMaker
//
//  Created by Vlad on 03.04.2021.
//

//
//  ContainerProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 29.03.2021.
//

import UIKit

final class ProfileContainer {
    let viewController: UIViewController

    static func assemble(with context: ProfileContext) -> ProfileContainer {
        let viewController = ProfileViewController()
        let presenter = ProfilePresenter()
        let router = ProfileRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return ProfileContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct ProfileContext {}
