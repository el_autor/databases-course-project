//
//  ProfilePresenter.swift
//  BoutMaker
//
//  Created by Vlad on 03.04.2021.
//

import Foundation

final class ProfilePresenter {
    weak var view: ProfileViewInput?

    var router: ProfileRouterInput?

    private var user: UserInfoRaw?

    init() {}
}

extension ProfilePresenter: ProfileViewOutput {
    func didTapEditLoginImageView() {
        view?.showChangeLoginAlertController()
    }

    func didTapEditPasswordImageView() {
        view?.showChangePasswordAlertController()
    }

    func didEnteredNewLogin(login: String) {
        AuthService.shared.changeLogin(userId: UserDefaultsService.shared.getUserId(),
                                       newLogin: login) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                case .userAlreadyExist:
                    self?.view?.showAlert(title: "Oops!", message: "Username is busy")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            UserDefaultsService.shared.setUsername(username: login)
            self?.view?.setUsername(username: login)
        }
    }

    func didEnteredNewPassword(password: String) {
        AuthService.shared.changePassword(userId: UserDefaultsService.shared.getUserId(),
                                          newPassword: password) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }
        }
    }

    func didLoadView() {
        DataService.shared.getUserInfo(userId: UserDefaultsService.shared.getUserId()) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            guard let data = result.data else {
                return
            }

            self?.user = data
            self?.view?.setUsername(username: data.username)
            self?.view?.setPoints(points: String(data.points))
            self?.view?.endRefreshing()
        }
    }

    func didTapLogoutButton() {
        UserDefaultsService.shared.deAuthorize()
        router?.showLoginScreen()
    }
}
