//
//  RouterContainer.swift
//  BoutMaker
//
//  Created by Vlad on 03.04.2021.
//

import UIKit

final class ProfileRouter {
    weak var viewController: UIViewController?
}

extension ProfileRouter: ProfileRouterInput {
    func showLoginScreen() {
        guard let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) else {
            return
        }

        let container = LoginContainer.assemble(with: LoginContext())

        container.viewController.modalPresentationStyle = .fullScreen

        let navigationVC = UINavigationController(rootViewController: container.viewController)

        navigationVC.isNavigationBarHidden = true
        navigationVC.modalPresentationStyle = .fullScreen

        window.rootViewController = navigationVC
    }
}
