//
//  ProfileProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 03.04.2021.
//

import Foundation

protocol ProfileViewInput: AnyObject {
    func showAlert(title: String, message: String)

    func setUsername(username: String)

    func setPoints(points: String)

    func endRefreshing()

    func showChangeLoginAlertController()

    func showChangePasswordAlertController()
}

protocol ProfileViewOutput: AnyObject {
    func didLoadView()

    func didTapLogoutButton()

    func didTapEditLoginImageView()

    func didTapEditPasswordImageView()

    func didEnteredNewLogin(login: String)

    func didEnteredNewPassword(password: String)
}

protocol ProfileRouterInput: AnyObject {
    func showLoginScreen()
}
