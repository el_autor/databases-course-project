//
//  ProfileViewController.swift
//  BoutMaker
//
//  Created by Vlad on 03.04.2021.
//

import UIKit
import PinLayout

final class ProfileViewController: UIViewController {
    var output: ProfileViewOutput?

    // MARK: UI elements

    private weak var scrollView: UIScrollView!

    private weak var titleLabel: UILabel!

    private weak var usernameBoxView: UIView!

    private weak var usernameBoxTitle: UILabel!

    private weak var usernameBoxContent: UILabel!

    private weak var usernameEditIconImageView: UIImageView!

    private weak var passwordBoxView: UIView!

    private weak var passwordBoxTitle: UILabel!

    private weak var passwordBoxContent: UILabel!

    private weak var passwordEditIconImageView: UIImageView!

    private weak var pointsBoxView: UIView!

    private weak var pointsTitleLabel: UILabel!

    private weak var pointsLabel: UILabel!

    private weak var logoutButton: UIButton!

    // MARK: Setup

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    private func setupView() {
        view.backgroundColor = UIColor.supportColor
    }

    private func setupSubviews() {
        setupScrollView()
        setupTitleLabel()
        setupUsernameBoxView()
        setupUsernameBoxTitle()
        setupUsernameBoxContent()
        setupUsernameEditIconImageView()
        setupPasswordBoxView()
        setupPasswordBoxTitle()
        setupPasswordBoxContent()
        setupPasswordEditIconImageView()
        setupPointsBoxView()
        setupPointsTitleLabel()
        setupPointsLabel()
        setupLogoutButton()
    }

    private func setupScrollView() {
        let scroller = UIScrollView()

        scroller.alwaysBounceVertical = true
        scroller.contentInsetAdjustmentBehavior = .never
        scroller.contentInset = UIEdgeInsets(top: 6%.of(UIScreen.main.bounds.height),
                                             left: 0,
                                             bottom: 0,
                                             right: 0)

        let refreshControl = UIRefreshControl()

        refreshControl.addTarget(self, action: #selector(didRequestRefresh), for: .valueChanged)
        scroller.refreshControl = refreshControl

        view.addSubview(scroller)
        scrollView = scroller
    }

    private func setupTitleLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 25,
                                        textColor: .white,
                                        text: "Profile")

        scrollView.addSubview(label)
        titleLabel = label
    }

    private func setupUsernameBoxView() {
        let boxView = UIView()

        boxView.layer.cornerRadius = 15
        boxView.backgroundColor = UIColor.mainColor

        scrollView.addSubview(boxView)
        usernameBoxView = boxView
    }

    private func setupUsernameBoxTitle() {
        let label = UILabel.customLabel(fontName: "DMSans-Bold",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: "Username")

        usernameBoxView.addSubview(label)
        usernameBoxTitle = label
    }

    private func setupUsernameBoxContent() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 18,
                                        textColor: .white,
                                        text: String())

        usernameBoxView.addSubview(label)
        usernameBoxContent = label
    }

    private func setupUsernameEditIconImageView() {
        let imageView = UIImageView()

        imageView.image = UIImage(systemName: "square.and.pencil",
                                  withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTapEditLoginImageView))

        recognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(recognizer)
        imageView.isUserInteractionEnabled = true

        usernameBoxView.addSubview(imageView)
        usernameEditIconImageView = imageView
    }

    private func setupPasswordBoxView() {
        let boxView = UIView()

        boxView.layer.cornerRadius = 15
        boxView.backgroundColor = UIColor.mainColor

        scrollView.addSubview(boxView)
        passwordBoxView = boxView
    }

    private func setupPasswordBoxTitle() {
        let label = UILabel.customLabel(fontName: "DMSans-Bold",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: "Password")

        passwordBoxView.addSubview(label)
        passwordBoxTitle = label
    }

    private func setupPasswordBoxContent() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 18,
                                        textColor: .white,
                                        text: "********")

        passwordBoxView.addSubview(label)
        passwordBoxContent = label
    }

    private func setupPasswordEditIconImageView() {
        let imageView = UIImageView()

        imageView.image = UIImage(systemName: "square.and.pencil",
                                  withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTapEditPasswordImageView))

        recognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(recognizer)
        imageView.isUserInteractionEnabled = true

        passwordBoxView.addSubview(imageView)
        passwordEditIconImageView = imageView
    }

    private func setupPointsBoxView() {
        let boxView = UIView()

        boxView.layer.cornerRadius = 15
        boxView.backgroundColor = UIColor.mainColor

        scrollView.addSubview(boxView)
        pointsBoxView = boxView
    }

    private func setupPointsTitleLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Bold",
                                        fontSize: 20,
                                        textColor: .white,
                                        textAlignment: .left,
                                        text: "Points")

        pointsBoxView.addSubview(label)
        pointsTitleLabel = label
    }

    private func setupPointsLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 18,
                                        textColor: .white,
                                        textAlignment: .right,
                                        text: String())

        pointsBoxView.addSubview(label)
        pointsLabel = label
    }

    private func setupLogoutButton() {
        let button = UIButton.customButton(title: "Log Out",
                                           backgroundColor: UIColor.mainColor,
                                           cornerRadius: 10,
                                           fontName: "DMSans-Bold",
                                           fontSize: 16,
                                           fontColor: .white)

        button.addTarget(self, action: #selector(didTapLogoutButton), for: .touchUpInside)

        scrollView.addSubview(button)
        logoutButton = button
    }

    // MARK: Layout

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutScrollView()
        layoutTitleLabel()
        layoutUsernameBoxView()
        layoutUsernameBoxTitle()
        layoutUsernameBoxContent()
        layoutUsernameEditIconImageView()
        layoutPasswordBoxView()
        layoutPasswordBoxTitle()
        layoutPasswordBoxContent()
        layoutPasswordEditIconImageView()
        layoutPointsBoxView()
        layoutPointsTitleLabel()
        layoutPointsLabel()
        layoutLogoutButton()
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .top()
            .height(5%)
            .hCenter()
            .width(100%)
    }

    private func layoutUsernameBoxView() {
        usernameBoxView.pin
            .below(of: titleLabel)
            .marginTop(1%)
            .left(5%)
            .width(43%)

        usernameBoxView.pin
            .height(usernameBoxView.bounds.width)
    }

    private func layoutUsernameBoxTitle() {
        usernameBoxTitle.pin
            .top()
            .height(30%)
            .hCenter()
            .width(100%)
    }

    private func layoutUsernameBoxContent() {
        usernameBoxContent.pin
            .below(of: usernameBoxTitle)
            .width(100%)
            .hCenter()
            .height(30%)
    }

    private func layoutUsernameEditIconImageView() {
        usernameEditIconImageView.pin
            .below(of: usernameBoxContent)
            .marginTop(12%)
            .bottom()
            .marginBottom(12%)

        usernameEditIconImageView.pin
            .width(usernameEditIconImageView.bounds.height)
            .hCenter()
    }

    private func layoutPasswordBoxView() {
        passwordBoxView.pin
            .below(of: titleLabel)
            .marginTop(1%)
            .right(5%)
            .width(43%)

        passwordBoxView.pin
            .height(passwordBoxView.bounds.width)
    }

    private func layoutPasswordBoxTitle() {
        passwordBoxTitle.pin
            .top()
            .height(30%)
            .hCenter()
            .width(100%)
    }

    private func layoutPasswordBoxContent() {
        passwordBoxContent.pin
            .below(of: passwordBoxTitle)
            .width(100%)
            .hCenter()
            .height(30%)
    }

    private func layoutPasswordEditIconImageView() {
        passwordEditIconImageView.pin
            .below(of: passwordBoxContent)
            .marginTop(12%)
            .bottom()
            .marginBottom(12%)

        passwordEditIconImageView.pin
            .width(passwordEditIconImageView.bounds.height)
            .hCenter()
    }

    private func layoutPointsBoxView() {
        pointsBoxView.pin
            .below(of: usernameBoxView)
            .marginTop(3%)
            .hCenter()
            .width(90%)
            .height(6%)
    }

    private func layoutPointsTitleLabel() {
        pointsTitleLabel.pin
            .top()
            .left(5%)
            .bottom()
            .width(30%)
    }

    private func layoutPointsLabel() {
        pointsLabel.pin
            .width(50%)
            .right(5%)
            .height(100%)
            .vCenter()
    }

    private func layoutScrollView() {
        scrollView.pin
            .all()
    }

    private func layoutLogoutButton() {
        logoutButton.pin
            .width(90%)
            .hCenter()
            .height(5.5%)
            .bottom((tabBarController?.tabBar.bounds.height ?? 0) + 15 + 6%.of(UIScreen.main.bounds.height))
    }

    // MARK: Action

    @objc
    private func didRequestRefresh() {
        output?.didLoadView()
    }

    @objc
    private func didTapLogoutButton() {
        output?.didTapLogoutButton()
    }

    @objc
    private func didTapEditLoginImageView() {
        output?.didTapEditLoginImageView()
    }

    @objc
    private func didTapEditPasswordImageView() {
        output?.didTapEditPasswordImageView()
    }
}

extension ProfileViewController: ProfileViewInput {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    func setUsername(username: String) {
        usernameBoxContent.text = username
    }

    func setPoints(points: String) {
        pointsLabel.text = points
    }

    func endRefreshing() {
        scrollView.refreshControl?.endRefreshing()
    }

    func showChangeLoginAlertController() {
        let alertController = UIAlertController(title: "Change login",
                                                message: String(),
                                                preferredStyle: .alert)

        alertController.addTextField { (textField: UITextField!) -> Void in
            textField.placeholder = "Enter new login"
        }

        let successAction = UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ -> Void in
            guard let newLogin = alertController.textFields?.first?.text else {
                return
            }

            self?.output?.didEnteredNewLogin(login: newLogin)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default)

        alertController.addAction(successAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }

    func showChangePasswordAlertController() {
        let alertController = UIAlertController(title: "Change password",
                                                message: String(),
                                                preferredStyle: .alert)

        alertController.addTextField { (textField: UITextField!) -> Void in
            textField.placeholder = "Enter new password"
            textField.isSecureTextEntry = true
        }

        alertController.addTextField { (textField: UITextField!) -> Void in
            textField.placeholder = "Repeat new password"
            textField.isSecureTextEntry = true
        }

        let successAction = UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ -> Void in
            guard let newPassword = alertController.textFields?.first?.text,
                  let repeatedPassword = alertController.textFields?[1].text,
                  newPassword == repeatedPassword,
                  newPassword.count > 5 else {
                return
            }

            self?.output?.didEnteredNewPassword(password: newPassword)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default)

        alertController.addAction(successAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}
