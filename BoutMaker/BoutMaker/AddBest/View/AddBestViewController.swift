//
//  AddBestViewController.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit
import PinLayout

final class AddBestViewController: UIViewController {
    var output: AddBestViewOutput?

    // MARK: UI elements

    private weak var scrollView: UIScrollView!

    private weak var titleLabel: UILabel!

    private weak var goBackImageView: UIImageView!

    private weak var firstFighterBoxView: UIView!

    private weak var firstFighterBoxUpLabel: UILabel!

    private weak var firstFighterBoxDownLabel: UILabel!

    private weak var firstFighterBoxRecordLabel: UILabel!

    private weak var separatorLabel: UILabel!

    private weak var secondFighterBoxView: UIView!

    private weak var secondFighterBoxUpLabel: UILabel!

    private weak var secondFighterBoxDownLabel: UILabel!

    private weak var secondFighterBoxRecordLabel: UILabel!

    private weak var addButton: UIButton!

    // MARK: Setup

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        output?.didLoadView()
    }

    private func setupView() {
        view.backgroundColor = UIColor.supportColor
    }

    private func setupSubviews() {
        setupScrollView()
        setupTitleLabel()
        setupGoBackImageView()
        setupFirstFighterBoxView()
        setupFirstFighterBoxUpLabel()
        setupFirstFighterBoxDownLabel()
        setupFirstFighterBoxRecordLabel()
        setupSeparatorLabel()
        setupSecondFighterBoxView()
        setupSecondFighterBoxUpLabel()
        setupSecondFighterBoxDownLabel()
        setupSecondFighterBoxRecordLabel()
        setupAddButton()
    }

    private func setupScrollView() {
        let scroller = UIScrollView()

        scroller.alwaysBounceVertical = true
        scroller.contentInsetAdjustmentBehavior = .never

        view.addSubview(scroller)
        scrollView = scroller
    }

    private func setupTitleLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 25,
                                        textColor: .white,
                                        text: "Add your bout")

        scrollView.addSubview(label)
        titleLabel = label
    }

    private func setupGoBackImageView() {
        let imageView = UIImageView()

        imageView.image = UIImage(systemName: "chevron.backward",
                                  withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white

        let tapRecognizer = UITapGestureRecognizer(target: self,
                                                   action: #selector(didTapBackImageView))
        tapRecognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tapRecognizer)
        imageView.isUserInteractionEnabled = true

        scrollView.addSubview(imageView)
        goBackImageView = imageView
    }

    private func setupFirstFighterBoxView() {
        let boxView = UIView()

        boxView.backgroundColor = UIColor.mainColor
        boxView.layer.cornerRadius = 15

        let tapRecognizer = UITapGestureRecognizer(target: self,
                                                   action: #selector(didTapFirstBoxView))
        tapRecognizer.numberOfTapsRequired = 1
        boxView.addGestureRecognizer(tapRecognizer)

        scrollView.addSubview(boxView)
        firstFighterBoxView = boxView
    }

    private func setupFirstFighterBoxUpLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: String())

        firstFighterBoxUpLabel = label
        firstFighterBoxView.addSubview(label)
    }

    private func setupFirstFighterBoxDownLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: String())

        firstFighterBoxDownLabel = label
        firstFighterBoxView.addSubview(label)
    }

    private func setupFirstFighterBoxRecordLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: String())

        firstFighterBoxRecordLabel = label
        firstFighterBoxView.addSubview(label)
    }

    private func setupSeparatorLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: "vs")

        scrollView.addSubview(label)
        separatorLabel = label
    }

    private func setupSecondFighterBoxView() {
        let boxView = UIView()

        boxView.backgroundColor = UIColor.mainColor
        boxView.layer.cornerRadius = 15

        let tapRecognizer = UITapGestureRecognizer(target: self,
                                                   action: #selector(didTapSecondBoxView))
        tapRecognizer.numberOfTapsRequired = 1
        boxView.addGestureRecognizer(tapRecognizer)

        scrollView.addSubview(boxView)
        secondFighterBoxView = boxView
    }

    private func setupSecondFighterBoxUpLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: String())

        secondFighterBoxUpLabel = label
        secondFighterBoxView.addSubview(label)
    }

    private func setupSecondFighterBoxDownLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: String())

        secondFighterBoxDownLabel = label
        secondFighterBoxView.addSubview(label)
    }

    private func setupSecondFighterBoxRecordLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: String())

        secondFighterBoxRecordLabel = label
        secondFighterBoxView.addSubview(label)
    }

    private func setupAddButton() {
        let button = UIButton.customButton(title: "Add",
                                           backgroundColor: UIColor.mainColor,
                                           cornerRadius: 10,
                                           fontName: "DMSans-Bold",
                                           fontSize: 16,
                                           fontColor: .white)

        button.addTarget(self,
                         action: #selector(didTapAddButton),
                         for: .touchUpInside)
        scrollView.addSubview(button)
        addButton = button
    }

    // MARK: Layout

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutScrollView()
        layoutTitleLabel()
        layoutGoBackImageView()
        layoutFirstFighterBoxView()
        layoutFirstFighterBoxUpLabel()
        layoutFirstFighterBoxDownLabel()
        layoutFirstFighterBoxRecordLabel()
        layoutSeparatorLabel()
        layoutSecondFighterBoxView()
        layoutSecondFighterBoxUpLabel()
        layoutSecondFighterBoxDownLabel()
        layoutSecondFighterBoxRecordLabel()
        layoutAddButton()
    }

    private func layoutScrollView() {
        scrollView.pin
            .all()
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .top(7%)
            .height(5%)
            .hCenter()
            .width(100%)
    }

    private func layoutGoBackImageView() {
        goBackImageView.pin
            .height(3%)

        goBackImageView.pin
            .top(titleLabel.frame.midY - goBackImageView.bounds.height / 2)
            .width(goBackImageView.bounds.height)
            .left(5%)
    }

    private func layoutFirstFighterBoxView() {
        firstFighterBoxView.pin
            .height(20%)
            .width(90%)
            .hCenter()
            .below(of: titleLabel)
            .marginTop(2%)
    }

    private func layoutFirstFighterBoxUpLabel() {
        firstFighterBoxUpLabel.pin
            .width(100%)
            .height(20%)
            .top(20%)
            .hCenter()
    }

    private func layoutFirstFighterBoxDownLabel() {
        firstFighterBoxDownLabel.pin
            .width(100%)
            .height(20%)
            .below(of: firstFighterBoxUpLabel)
            .hCenter()
    }

    private func layoutFirstFighterBoxRecordLabel() {
        firstFighterBoxRecordLabel.pin
            .width(100%)
            .height(20%)
            .below(of: firstFighterBoxDownLabel)
            .marginTop(5%)
            .hCenter()
    }

    private func layoutSeparatorLabel() {
        separatorLabel.pin
            .height(5%)
            .width(90%)
            .hCenter()
            .below(of: firstFighterBoxView)
            .marginTop(1%)
    }

    private func layoutSecondFighterBoxView() {
        secondFighterBoxView.pin
            .height(20%)
            .width(90%)
            .hCenter()
            .below(of: separatorLabel)
            .marginTop(1%)
    }

    private func layoutSecondFighterBoxUpLabel() {
        secondFighterBoxUpLabel.pin
            .width(100%)
            .height(20%)
            .top(20%)
            .hCenter()
    }

    private func layoutSecondFighterBoxDownLabel() {
        secondFighterBoxDownLabel.pin
            .width(100%)
            .height(20%)
            .below(of: secondFighterBoxUpLabel)
            .hCenter()
    }

    private func layoutSecondFighterBoxRecordLabel() {
        secondFighterBoxRecordLabel.pin
            .width(100%)
            .height(20%)
            .below(of: secondFighterBoxDownLabel)
            .marginTop(5%)
            .hCenter()
    }

    private func layoutAddButton() {
        addButton.pin
            .width(90%)
            .hCenter()
            .height(5.5%)
            .bottom((tabBarController?.tabBar.bounds.height ?? 0) + 15)
    }

    // MARK: Actions

    @objc
    private func didTapBackImageView() {
        output?.didTapBackImageView()
    }

    @objc
    private func didTapFirstBoxView() {
        output?.didTapFirstBoxView()
    }

    @objc
    private func didTapSecondBoxView() {
        output?.didTapSecondBoxView()
    }

    @objc
    private func didTapAddButton() {
        output?.didTapAddButton()
    }
}

extension AddBestViewController: AddBestViewInput {
    func setupDefaultFirstFighterBox() {
        firstFighterBoxUpLabel.text = "Tap & Choose"
        firstFighterBoxDownLabel.text = "Fighter"
        firstFighterBoxRecordLabel.isHidden = true
    }

    func setupDefaultSecondFighterBox() {
        secondFighterBoxUpLabel.text = "Tap & Choose"
        secondFighterBoxDownLabel.text = "Fighter"
        secondFighterBoxRecordLabel.isHidden = true
    }

    func setupFirstFighterBox(fighter: FighterRaw) {
        var winsString: String?
        var lossesString: String?
        var drawsString: String?

        if let wins = fighter.wins {
            winsString = String(wins)
        }

        if let losses = fighter.losses {
            lossesString = String(losses)
        }

        if let draws = fighter.draws {
            drawsString = String(draws)
        }

        firstFighterBoxUpLabel.text = fighter.firstName
        firstFighterBoxDownLabel.text = fighter.lastName
        firstFighterBoxRecordLabel.text = "\(winsString ?? "?")-\(lossesString ?? "?")-\(drawsString ?? "?")"
        firstFighterBoxRecordLabel.isHidden = false
    }

    func setupSecondFighterBox(fighter: FighterRaw) {
        var winsString: String?
        var lossesString: String?
        var drawsString: String?

        if let wins = fighter.wins {
            winsString = String(wins)
        }

        if let losses = fighter.losses {
            lossesString = String(losses)
        }

        if let draws = fighter.draws {
            drawsString = String(draws)
        }

        secondFighterBoxUpLabel.text = fighter.firstName
        secondFighterBoxDownLabel.text = fighter.lastName
        secondFighterBoxRecordLabel.text = "\(winsString ?? "?")-\(lossesString ?? "?")-\(drawsString ?? "?")"
        secondFighterBoxRecordLabel.isHidden = false
    }

    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }
}
