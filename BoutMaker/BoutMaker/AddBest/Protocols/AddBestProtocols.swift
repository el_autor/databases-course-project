//
//  AddBestProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

protocol AddBestViewInput: AnyObject {
    func showAlert(title: String, message: String)

    func setupDefaultFirstFighterBox()

    func setupDefaultSecondFighterBox()

    func setupFirstFighterBox(fighter: FighterRaw)

    func setupSecondFighterBox(fighter: FighterRaw)
}

protocol AddBestViewOutput: AnyObject {
    func didTapBackImageView()

    func didTapFirstBoxView()

    func didTapSecondBoxView()

    func didLoadView()

    func didTapAddButton()
}

protocol AddBestRouterInput: AnyObject {
    func goBack()

    func showChooseFighterScreen(onChoose: @escaping ((FighterRaw) -> Void))
}
