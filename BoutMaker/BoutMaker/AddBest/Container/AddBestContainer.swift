//
//  AddBestContainer.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit

final class AddBestContainer {
    let viewController: UIViewController

    static func assemble(with context: AddBestContext) -> AddBestContainer {
        let viewController = AddBestViewController()
        let presenter = AddBestPresenter()
        let router = AddBestRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return AddBestContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct AddBestContext {}
