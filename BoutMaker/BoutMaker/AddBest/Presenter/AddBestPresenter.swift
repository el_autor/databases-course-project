//
//  AddBestPresenter.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import Foundation

final class AddBestPresenter {
    weak var view: AddBestViewInput?

    var router: AddBestRouterInput?

    private var firstFighter: FighterRaw?

    private var secondFighter: FighterRaw?

    init() {}
}

extension AddBestPresenter: AddBestViewOutput {
    func didTapBackImageView() {
        router?.goBack()
    }

    func didTapFirstBoxView() {
        router?.showChooseFighterScreen { [weak self] fighter in
            self?.firstFighter = fighter
        }
    }

    func didTapSecondBoxView() {
        router?.showChooseFighterScreen { [weak self] fighter in
            self?.secondFighter = fighter
        }
    }

    func didLoadView() {
        if let first = firstFighter {
            view?.setupFirstFighterBox(fighter: first)
        } else {
            view?.setupDefaultFirstFighterBox()
        }

        if let second = secondFighter {
            view?.setupSecondFighterBox(fighter: second)
        } else {
            view?.setupDefaultSecondFighterBox()
        }
    }

    func didTapAddButton() {
        if let first = firstFighter,
           let second = secondFighter {
            DataService.shared.addBestFight(idFighterA: first.id,
                                            idFighterB: second.id,
                                            nameFighterA: "\(first.firstName) \(first.lastName)",
                                            nameFighterB: "\(second.firstName) \(second.lastName)") { [weak self] result in
                guard result.error == nil else {
                    guard let networkError = result.error else {
                        return
                    }

                    switch networkError {
                    case .networkNotReachable:
                        self?.view?.showAlert(title: "Oops!", message: "No connection")
                    default:
                        self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                    }

                    return
                }

                self?.router?.goBack()
            }
        } else {
            router?.goBack()
        }
    }
}
