//
//  AddBestRouter.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit

final class AddBestRouter {
    weak var viewController: UIViewController?
}

extension AddBestRouter: AddBestRouterInput {
    func goBack() {
        viewController?.navigationController?.popViewController(animated: true)
    }

    func showChooseFighterScreen(onChoose: @escaping ((FighterRaw) -> Void)) {
        let container = ChooseFighterContainer.assemble(with: ChooseFighterContext(onChoose: onChoose))

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.navigationController?.pushViewController(container.viewController,
                                                                 animated: true)
    }
}
