//
//  UserDefaultsServiceProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 27.03.2021.
//

protocol UserDefaultsServiceInput {
    func isAuthorized() -> Bool

    func getUsername() -> String?

    func getPassword() -> String?

    func getUserId() -> Int

    func getUserRole() -> String?

    func authorize(username: String, password: String, userId: Int)

    func deAuthorize()

    func setUserRole(role: String)

    func setUsername(username: String)
}
