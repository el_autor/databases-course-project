//
//  UserDefaultsService.swift
//  BoutMaker
//
//  Created by Vlad on 27.03.2021.
//

import Foundation

final class UserDefaultsService {
    static let shared = UserDefaultsService()

    private let dbBaseURL = "https://32b8b5f457fc.ngrok.io/"

    private let dbKey = "3ccfe00f-c035-4cbd-8346-921cafaa3b99"

    private let apiBaseURL = "https://fly.sportsdata.io/v3/mma/"

    private let apiKey = "423809b18123465bac092de4971ab193"

    private init() {
        // save in UserDefaults
    }

    func getDatabaseBaseURL() -> String {
        return dbBaseURL
    }

    func getApiBaseURL() -> String {
        return apiBaseURL
    }

    func getAPIKey() -> String {
        return apiKey
    }

    func dbAPIKey() -> String {
        return dbKey
    }
}

extension UserDefaultsService: UserDefaultsServiceInput {
    func isAuthorized() -> Bool {
        return UserDefaults.standard.bool(forKey: Constants.authKey)
    }

    func getUsername() -> String? {
        return UserDefaults.standard.string(forKey: Constants.usernameKey)
    }

    func getPassword() -> String? {
        return UserDefaults.standard.string(forKey: Constants.passwordKey)
    }

    func getUserId() -> Int {
        return UserDefaults.standard.integer(forKey: Constants.userIdKey)
    }

    func getUserRole() -> String? {
        return UserDefaults.standard.string(forKey: Constants.roleKey)
    }

    func authorize(username: String, password: String, userId: Int) {
        UserDefaults.standard.setValue(true, forKey: Constants.authKey)
        UserDefaults.standard.setValue(username, forKey: Constants.usernameKey)
        UserDefaults.standard.setValue(password, forKey: Constants.passwordKey)
        UserDefaults.standard.setValue(userId, forKey: Constants.userIdKey)
    }

    func deAuthorize() {
        UserDefaults.standard.setValue(false, forKey: Constants.authKey)
    }

    func setUserRole(role: String) {
        UserDefaults.standard.setValue(role, forKey: Constants.roleKey)
    }

    func setUsername(username: String) {
        UserDefaults.standard.setValue(username, forKey: Constants.usernameKey)
    }
}

extension UserDefaultsService {
    private struct Constants {
        static let authKey: String = "isAuthorized"

        static let usernameKey: String = "username"

        static let passwordKey: String = "password"

        static let userIdKey: String = "id"

        static let roleKey: String = "role"
    }
}
