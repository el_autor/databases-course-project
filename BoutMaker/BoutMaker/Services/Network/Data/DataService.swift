//
//  DataService.swift
//  BoutMaker
//
//  Created by Vlad on 22.03.2021.
//

import Alamofire
import Foundation

final class DataService {
    static let shared = DataService()

    private init() { }
}

extension DataService: DataServiceInput {
    func saveFanForFighter(userId: Int, fighterId: Int, name: String, completion: @escaping (SingleResult<NetworkError>) -> Void) {
        let parameters: [String: Any] = [
            "user_id": userId,
            "fighter_id": fighterId,
            "name": name
        ]

        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())createFan",
                                 method: .post,
                                 parameters: parameters)

        var result = SingleResult<NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.response { response in
            switch response.result {
            case .success:
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    completion(result)
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }

    }

    func deleteFanForFighter(userId: Int, fighterId: Int, completion: @escaping (SingleResult<NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())deleteFan?user_id=\(userId)&fighter_id=\(fighterId)")

        var result = SingleResult<NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.response { response in
            switch response.result {
            case .success:
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    completion(result)
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func getFavouriteFightersForUser(userId: Int, completion: @escaping (Result<FavouriteFightersRaw, NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())getFightersForFan?user_id=\(userId)")

        var result = Result<FavouriteFightersRaw, NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: FavouriteFightersRaw.self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func collectPoints(userId: Int, fightId: Int, completion: @escaping (SingleResult<NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())collectPoints?user_id=\(userId)&fight_id=\(fightId)")

        var result = SingleResult<NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.response { response in
            switch response.result {
            case .success:
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    completion(result)
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func getSchedule(completion: @escaping (Result<[EventRaw], NetworkError>) -> Void) {
        let year = Calendar.current.component(.year, from: Date())
        let request = AF.request("\(UserDefaultsService.shared.getApiBaseURL())scores/json/Schedule/UFC/\(year)?key=\(UserDefaultsService.shared.getAPIKey())")
        var result = Result<[EventRaw], NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: [EventRaw].self) { response in
            switch response.result {
            case .success(let data):
                result.data = data
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
            }

            completion(result)
        }
    }

    func getPopularFighters(completion: @escaping (Result<[PopularFighterRaw], NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())getPopularFighters")

        var result = Result<[PopularFighterRaw], NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: [PopularFighterRaw].self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func getLeaderboard(completion: @escaping (Result<[UserRaw], NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())leaderboard")

        var result = Result<[UserRaw], NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: [UserRaw].self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func getFighters(completion: @escaping (Result<[FighterRaw], NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getApiBaseURL())scores/json/Fighters?key=\(UserDefaultsService.shared.getAPIKey())")

        var result = Result<[FighterRaw], NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: [FighterRaw].self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func addBestFight(idFighterA: Int,
                      idFighterB: Int,
                      nameFighterA: String,
                      nameFighterB: String,
                      completion: @escaping (SingleResult<NetworkError>) -> Void) {
        let parameters: [String: String] = [
            "idFighterA": "\(idFighterA)",
            "idFighterB": "\(idFighterB)",
            "nameFighterA": nameFighterA,
            "nameFighterB": nameFighterB
        ]

        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())addBestFight",
                                 method: .post,
                                 parameters: parameters)

        var result = SingleResult<NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.response { response in
            switch response.result {
            case .success:
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    completion(result)
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func deleteBestFight(id: Int, completion: @escaping (SingleResult<NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())deleteBestFight?id=\(id)")

        var result = SingleResult<NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.response { response in
            switch response.result {
            case .success:
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    completion(result)
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func getBestFights(completion: @escaping (Result<[BestFightRaw], NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())bestFights")

        var result = Result<[BestFightRaw], NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: [BestFightRaw].self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func getEventInfo(eventId: Int, completion: @escaping (Result<EventFullInfoRaw, NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getApiBaseURL())scores/json/Event/\(eventId)?key=\(UserDefaultsService.shared.getAPIKey())")

        var result = Result<EventFullInfoRaw, NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: EventFullInfoRaw.self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func getSavedEventInfo(eventId: Int, userId: Int, completion: @escaping (Result<[BoutSavedResultRaw], NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())getEventResults?event_id=\(eventId)&user_id=\(userId)")

        var result = Result<[BoutSavedResultRaw], NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: [BoutSavedResultRaw].self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func setFightResult(userId: Int,
                        fighterIdA: Int,
                        fighterIdB: Int,
                        winnerId: Int?,
                        fightId: Int,
                        eventId: Int,
                        completion: @escaping (SingleResult<NetworkError>) -> Void) {
        var parameters: [String: Any] = [
            "user_id": userId,
            "fighter_id_a": fighterIdA,
            "fighter_id_b": fighterIdB,
            "fight_id": fightId,
            "event_id": eventId
        ]

        if let id = winnerId {
            parameters["winner_id"] = id
        }

        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())setFightResult",
                                 method: .post,
                                 parameters: parameters)

        var result = SingleResult<NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.response { response in
            switch response.result {
            case .success:
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    completion(result)
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }

    }

    func getUserInfo(userId: Int, completion: @escaping (Result<UserInfoRaw, NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())accountOwnerInfo?user_id=\(userId)&apikey=\(UserDefaultsService.shared.dbAPIKey())")

        var result = Result<UserInfoRaw, NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: UserInfoRaw.self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func getFightStats(fightId: Int, completion: @escaping (Result<FightStatsRaw, NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())fightPercentage?fight_id=\(fightId)")

        var result = Result<FightStatsRaw, NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: FightStatsRaw.self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }
}
