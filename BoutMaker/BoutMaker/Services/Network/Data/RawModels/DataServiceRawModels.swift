//
//  DataServiceRawModels.swift
//  BoutMaker
//
//  Created by Vlad on 28.03.2021.
//

import Foundation

struct RegisterResult: Decodable {
    let userId: Int
}

struct LoginResult: Decodable {
    let userId: Int

    let role: String
}

struct EventRaw: Decodable {
    let id: Int

    let name: String

    let shortName: String

    let datetime: String

    let status: String

    private enum CodingKeys: String, CodingKey {
        case id = "EventId"
        case name = "Name"
        case shortName = "ShortName"
        case datetime = "DateTime"
        case status = "Status"
    }
}

struct UserRaw: Decodable {
    let points: Int

    let username: String

    let role: String
}

struct PopularFighterRaw: Decodable {
    let name: String

    let fans: Int
}

struct UserInfoRaw: Decodable {
    let username: String

    let password: String

    let points: Int
}

struct FighterRaw: Decodable {
    let id: Int

    let firstName: String

    let lastName: String

    let weightclass: String?

    let wins: Int?

    let losses: Int?

    let draws: Int?

    let noContest: Int?

    private enum CodingKeys: String, CodingKey {
        case id = "FighterId"
        case firstName = "FirstName"
        case lastName = "LastName"
        case weightclass = "WeightClass"
        case wins = "Wins"
        case losses = "Losses"
        case draws = "Draws"
        case noContest = "NoContests"
    }
}

struct BestFightRaw: Decodable {
    let nameFighterA: String

    let nameFighterB: String

    let voices: String

    let id: Int
}

struct FavouriteFightersRaw: Decodable {
    let fighterIds: [Int]
}

struct FightStatsRaw: Decodable {
    let firstFighterPercentage: String

    let secondFighterPercentage: String
}

struct EventFullInfoRaw: Decodable {

    struct BoutRaw: Decodable {

        struct FighterRaw: Decodable {
            let id: Int

            let firstname: String

            let lastname: String

            let wins: Int

            let losses: Int

            let draws: Int

            private enum CodingKeys: String, CodingKey {
                case id = "FighterId"
                case firstname = "FirstName"
                case lastname = "LastName"
                case wins = "PreFightWins"
                case losses = "PreFightLosses"
                case draws = "PreFightDraws"
            }
        }

        let id: Int

        let weightCategory: String?

        let status: String?

        let winnerId: Int?

        let fighters: [FighterRaw]

        private enum CodingKeys: String, CodingKey {
            case id = "FightId"
            case weightCategory = "WeightClass"
            case status = "Status"
            case winnerId = "WinnerId"
            case fighters = "Fighters"
        }
    }

    let bouts: [BoutRaw]

    private enum CodingKeys: String, CodingKey {
        case bouts = "Fights"
    }
}

struct BoutSavedResultRaw: Decodable {
    let fighterIdA: Int

    let fighterIdB: Int

    let fightId: Int

    let winnerId: Int

    let pointsAreCollected: Bool
}
