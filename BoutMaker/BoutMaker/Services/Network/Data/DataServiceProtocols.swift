//
//  DataServiceProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 22.03.2021.
//

protocol DataServiceInput {
    func getSchedule(completion: @escaping (Result<[EventRaw], NetworkError>) -> Void)

    func getLeaderboard(completion: @escaping (Result<[UserRaw], NetworkError>) -> Void)

    func getPopularFighters(completion: @escaping (Result<[PopularFighterRaw], NetworkError>) -> Void)

    func getFighters(completion: @escaping (Result<[FighterRaw], NetworkError>) -> Void)

    func addBestFight(idFighterA: Int,
                      idFighterB: Int,
                      nameFighterA: String,
                      nameFighterB: String,
                      completion: @escaping (SingleResult<NetworkError>) -> Void)

    func deleteBestFight(id: Int, completion: @escaping (SingleResult<NetworkError>) -> Void)

    func getBestFights(completion: @escaping (Result<[BestFightRaw], NetworkError>) -> Void)

    func getEventInfo(eventId: Int, completion: @escaping (Result<EventFullInfoRaw, NetworkError>) -> Void)

    func saveFanForFighter(userId: Int, fighterId: Int, name: String, completion: @escaping (SingleResult<NetworkError>) -> Void)

    func deleteFanForFighter(userId: Int, fighterId: Int, completion: @escaping (SingleResult<NetworkError>) -> Void)

    func getSavedEventInfo(eventId: Int, userId: Int, completion: @escaping (Result<[BoutSavedResultRaw], NetworkError>) -> Void)

    func setFightResult(userId: Int,
                        fighterIdA: Int,
                        fighterIdB: Int,
                        winnerId: Int?,
                        fightId: Int,
                        eventId: Int,
                        completion: @escaping (SingleResult<NetworkError>) -> Void)

    func getUserInfo(userId: Int, completion: @escaping (Result<UserInfoRaw, NetworkError>) -> Void)

    func collectPoints(userId: Int, fightId: Int, completion: @escaping (SingleResult<NetworkError>) -> Void)

    func getFightStats(fightId: Int, completion: @escaping (Result<FightStatsRaw, NetworkError>) -> Void)

    func getFavouriteFightersForUser(userId: Int, completion: @escaping (Result<FavouriteFightersRaw, NetworkError>) -> Void)
}
