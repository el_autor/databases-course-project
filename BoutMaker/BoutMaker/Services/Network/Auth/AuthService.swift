//
//  AuthService.swift
//  BoutMaker
//
//  Created by Vlad on 22.03.2021.
//

import Alamofire

final class AuthService {
    static let shared = AuthService()

    private init() { }
}

extension AuthService: AuthServiceInput {
    func register(username: String,
                  password: String,
                  completion: @escaping (Result<RegisterResult, NetworkError>) -> Void) {
        var result = Result<RegisterResult, NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        let parameters: [String: String] =
        [
            "username": username,
            "password": password
        ]

        let url = UserDefaultsService.shared.getDatabaseBaseURL() + "register?apikey=\(UserDefaultsService.shared.dbAPIKey())"
        let request = AF.request(url, method: .post, parameters: parameters)

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: RegisterResult.self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else if error.isResponseSerializationError {
                    result.error = .userAlreadyExist
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func login(username: String,
               password: String,
               completion: @escaping (Result<LoginResult, NetworkError>) -> Void) {
        let request = AF.request("\(UserDefaultsService.shared.getDatabaseBaseURL())login?username=\(username)&password=\(password)&apikey=\(UserDefaultsService.shared.dbAPIKey())")
        var result = Result<LoginResult, NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.responseDecodable(of: LoginResult.self) { response in
            switch response.result {
            case .success(let data):
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    result.data = data
                    completion(result)
                default:
                    result.error = .userNotExist
                    completion(result)
                }

            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else if error.isResponseSerializationError {
                    result.error = .userNotExist
                } else {
                    result.error = .unknownError
                }

                completion(result)
            }
        }
    }

    func setRole(username: String,
                 role: String,
                 completion: @escaping (SingleResult<NetworkError>) -> Void) {
        var result = SingleResult<NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        let parameters: [String: String] =
        [
            "username": username,
            "role": role
        ]

        let url = UserDefaultsService.shared.getDatabaseBaseURL() + "chooseRole?apikey=\(UserDefaultsService.shared.dbAPIKey())"
        let request = AF.request(url, method: .post, parameters: parameters)

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.response { response in
            switch response.result {
            case .success:
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func changeLogin(userId: Int,
                     newLogin: String,
                     completion: @escaping (SingleResult<NetworkError>) -> Void) {
        var result = SingleResult<NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        let parameters: [String: Any] =
        [
            "user_id": userId,
            "new_login": newLogin
        ]

        let url = UserDefaultsService.shared.getDatabaseBaseURL() + "changeLogin?apikey=\(UserDefaultsService.shared.dbAPIKey())"
        let request = AF.request(url, method: .post, parameters: parameters)

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.response { response in
            switch response.result {
            case .success:
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    completion(result)
                    return
                case 404:
                    result.error = .userAlreadyExist
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }
    }

    func changePassword(userId: Int,
                        newPassword: String,
                        completion: @escaping (SingleResult<NetworkError>) -> Void) {
        var result = SingleResult<NetworkError>()

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        let parameters: [String: Any] =
        [
            "user_id": userId,
            "new_password": newPassword
        ]

        let url = UserDefaultsService.shared.getDatabaseBaseURL() + "changePassword?apikey=\(UserDefaultsService.shared.dbAPIKey())"
        let request = AF.request(url, method: .post, parameters: parameters)

        guard NetworkReachabilityManager()?.isReachable ?? false else {
            result.error = .networkNotReachable
            completion(result)
            return
        }

        request.response { response in
            switch response.result {
            case .success:
                guard let statusCode = response.response?.statusCode else {
                    result.error = .unknownError
                    completion(result)
                    return
                }

                switch statusCode {
                case 200:
                    completion(result)
                    return
                default:
                    result.error = .unknownError
                    completion(result)
                    return
                }
            case .failure(let error):
                if error.isInvalidURLError {
                    result.error = .connectionToServerError
                } else {
                    result.error = .unknownError
                }
                completion(result)
            }
        }

    }
}
