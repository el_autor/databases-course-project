//
//  File.swift
//  BoutMaker
//
//  Created by Vlad on 22.03.2021.
//

protocol AuthServiceInput {
    func setRole(username: String,
                 role: String,
                 completion: @escaping (SingleResult<NetworkError>) -> Void)

    func register(username: String,
                  password: String,
                  completion: @escaping (Result<RegisterResult, NetworkError>) -> Void)

    func login(username: String,
               password: String,
               completion: @escaping (Result<LoginResult, NetworkError>) -> Void)

    func changeLogin(userId: Int,
                     newLogin: String,
                     completion: @escaping (SingleResult<NetworkError>) -> Void)

    func changePassword(userId: Int,
                        newPassword: String,
                        completion: @escaping (SingleResult<NetworkError>) -> Void)
}
