//
//  EventsProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 27.03.2021.
//

import Foundation

protocol EventsViewInput: AnyObject {
    func showAlert(title: String, message: String)

    func reloadData()
}

protocol EventsViewOutput: AnyObject {
    func didLoadView()

    func getEventsCount() -> Int

    func viewModel(index: Int) -> EventViewModel?

    func didSelectEvent(index: Int)
}

protocol EventsRouterInput: AnyObject {
    func showBoutsScreen(title: String, eventId: Int, userId: Int)
}
