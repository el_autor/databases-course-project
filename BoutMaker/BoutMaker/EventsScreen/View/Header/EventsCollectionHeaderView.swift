//
//  EventsCollectionHeaderView.swift
//  BoutMaker
//
//  Created by Vlad on 28.03.2021.
//

import UIKit
import PinLayout

final class EventsCollectionHeaderView: UICollectionReusableView {

    private weak var titleLabel: UILabel!

    static let reuseIdentifier = "eventsHeader"

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupSubviews()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutTitleLabel()
    }

    required init?(coder: NSCoder) {
        return nil
    }

    // MARK: Setup

    private func setupView() {

    }

    private func setupSubviews() {
        setupTitleLabel()
    }

    private func setupTitleLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 25,
                                        textColor: .white,
                                        text: "Schedule")

        addSubview(label)
        titleLabel = label
    }

    // MARK: Layout

    private func layoutTitleLabel() {
        titleLabel.pin
            .width(100%)
            .height(100%)
    }
}
