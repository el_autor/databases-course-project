//
//  EventsTableViewCell.swift
//  BoutMaker
//
//  Created by Vlad on 27.03.2021.
//

import UIKit
import PinLayout

final class EventsCollectionViewCell: UICollectionViewCell {

    static let cellIdentifier: String = "eventCell"

    // MARK: UI elements

    private weak var titleLabel: UILabel!

    private weak var leftFighterLabel: UILabel!

    private weak var separatorLabel: UILabel!

    private weak var rightFighterLabel: UILabel!

    private weak var dateLabel: UILabel!

    // MARK: Setup

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupSubviews()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutTitleLabel()
        layoutLeftFighterLabel()
        layoutSeparatorLabel()
        layoutRightFighterLabel()
        layoutDateLabel()
    }

    required init?(coder: NSCoder) {
        return nil
    }

    private func setupView() {
        backgroundColor = UIColor.mainColor
        layer.cornerRadius = 15
    }

    private func setupSubviews() {
        setupTitleLabel()
        setupLeftFighterLabel()
        setupSeparatorLabel()
        setupRightFighterLabel()
        setupDateLabel()
    }

    public func configure(model: EventViewModel) {
        titleLabel.text = model.name
        leftFighterLabel.text = model.fighterFirst
        rightFighterLabel.text = model.fighterSecond
        dateLabel.text = model.datetime
    }

    private func setupTitleLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: String())

        addSubview(label)
        titleLabel = label
    }

    private func setupLeftFighterLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: String())

        addSubview(label)
        leftFighterLabel = label
    }

    private func setupSeparatorLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: "vs")

        addSubview(label)
        separatorLabel = label
    }

    private func setupRightFighterLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: String())

        addSubview(label)
        rightFighterLabel = label
    }

    private func setupDateLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: String())

        addSubview(label)
        dateLabel = label
    }

    // MARK: Layout

    private func layoutTitleLabel() {
        titleLabel.pin
            .top(10%)
            .hCenter()
            .width(100%)
            .height(15%)
    }

    private func layoutLeftFighterLabel() {
        leftFighterLabel.pin
            .below(of: titleLabel)
            .marginTop(15%)
            .left(5%)
            .width(35%)
            .height(20%)
    }

    private func layoutSeparatorLabel() {
        separatorLabel.pin
            .below(of: titleLabel)
            .marginTop(20%)
            .hCenter()
            .width(10%)
            .height(10%)
    }

    private func layoutRightFighterLabel() {
        rightFighterLabel.pin
            .below(of: titleLabel)
            .marginTop(15%)
            .right(5%)
            .width(35%)
            .height(20%)
    }

    private func layoutDateLabel() {
        dateLabel.pin
            .below(of: separatorLabel)
            .marginTop(20%)
            .hCenter()
            .width(100%)
            .height(10%)
    }
}
