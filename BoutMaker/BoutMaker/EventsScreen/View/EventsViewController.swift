//
//  EventsViewController.swift
//  BoutMaker
//
//  Created by Vlad on 27.03.2021.
//

import UIKit
import PinLayout

final class EventsViewController: UIViewController {
    var output: EventsViewOutput?

    private weak var eventsCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutEventsCollectionView()
    }

    private func setupView() {
        view.backgroundColor = UIColor.supportColor
    }

    private func setupSubviews() {
        setupEventsCollectionView()
    }

    private func setupEventsCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()

        flowLayout.itemSize = CGSize(width: 90%.of(UIScreen.main.bounds.width),
                                     height: 20%.of(UIScreen.main.bounds.height))

        let collectionView = UICollectionView(frame: .null,
                                              collectionViewLayout: flowLayout)

        collectionView.backgroundColor = UIColor.supportColor
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(EventsCollectionViewCell.self,
                                forCellWithReuseIdentifier: EventsCollectionViewCell.cellIdentifier)
        collectionView.register(EventsCollectionHeaderView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: EventsCollectionHeaderView.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self

        let refreshControl = UIRefreshControl()

        refreshControl.addTarget(self,
                                 action: #selector(didSwipe), for: .valueChanged)
        collectionView.refreshControl = refreshControl

        view.addSubview(collectionView)
        eventsCollectionView = collectionView
    }

    // MARK: Layout

    private func layoutEventsCollectionView() {
        eventsCollectionView.pin
            .top()
            .hCenter()
            .width(90%)
            .bottom()
    }

    // MARK: Actions

    @objc
    private func didSwipe() {
        output?.didLoadView()
    }

}

extension EventsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output?.didSelectEvent(index: indexPath.row)
    }
}

extension EventsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 90%.of(UIScreen.main.bounds.width),
                      height: 7%.of(UIScreen.main.bounds.height))
    }
}

extension EventsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: EventsCollectionHeaderView.reuseIdentifier,
                                                                             for: indexPath)

            return headerView
        }

        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getEventsCount() ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EventsCollectionViewCell.cellIdentifier, for: indexPath) as? EventsCollectionViewCell else {
            return UICollectionViewCell()
        }

        guard let viewModel = output?.viewModel(index: indexPath.row) else {
            return cell
        }

        cell.configure(model: viewModel)

        return cell
    }
}

extension EventsViewController: EventsViewInput {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    func reloadData() {
        eventsCollectionView.refreshControl?.endRefreshing()
        eventsCollectionView.reloadData()
    }
}
