//
//  EventsContainer.swift
//  BoutMaker
//
//  Created by Vlad on 27.03.2021.
//

import UIKit

final class EventsContainer {
    let viewController: UIViewController

    static func assemble(with context: EventsContext) -> EventsContainer {
        let viewController = EventsViewController()
        let presenter = EventsPresenter()
        let router = EventsRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return EventsContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct EventsContext {}
