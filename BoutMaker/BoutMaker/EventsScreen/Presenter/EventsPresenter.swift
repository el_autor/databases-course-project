//
//  EventsPresenter.swift
//  BoutMaker
//
//  Created by Vlad on 27.03.2021.
//

import Foundation

struct EventViewModel {
    let id: Int

    let name: String

    let fighterFirst: String

    let fighterSecond: String

    let datetime: String
}

final class EventsPresenter {
    private struct Model {
        var events: [EventViewModel]
    }

    private let monthes: [Int: String] = [
        1: "January",
        2: "February",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December"
    ]

    private var model: Model?

    weak var view: EventsViewInput?

    var router: EventsRouterInput?

    init() {}

    private func transform(rawModel: EventRaw) -> EventViewModel {
        var fighterLeft: String!
        var fighterRight: String!

        let wordsFullName = rawModel.name.split(separator: " ")

        if wordsFullName.count < 5 {
            fighterLeft = "Unknown"
            fighterRight = "Unknown"
        } else {
            if wordsFullName[1] == "Fight" {
                fighterLeft = String(wordsFullName[3])
                fighterRight = String(wordsFullName[5])
            } else {
                fighterLeft = String(wordsFullName[2])
                fighterRight = String(wordsFullName[4])
            }
        }

        let datetimeWords = rawModel.datetime.split(separator: "-")

        let year = String(datetimeWords[0])
        let month = String(datetimeWords[1])
        let day = String(datetimeWords[2][...datetimeWords[2].index(datetimeWords[2].startIndex, offsetBy: 1)])

        return EventViewModel(id: rawModel.id,
                              name: rawModel.shortName,
                              fighterFirst: fighterLeft,
                              fighterSecond: fighterRight,
                              datetime: day + " \(monthes[Int(month) ?? 1] ?? "Unknown"), " + year)
    }

    private func isFromPast(date: String) -> Bool {
        let currentDate = Date()
        let currentYear = Calendar.current.component(.year, from: currentDate)
        let currentMonth = Calendar.current.component(.month, from: currentDate)
        let currentDay = Calendar.current.component(.day, from: currentDate)

        let datetimeWords = date.split(separator: "-")

        let year = Int(datetimeWords[0]) ?? 0
        let month = Int(datetimeWords[1]) ?? 0
        let day = Int(datetimeWords[2][...datetimeWords[2].index(datetimeWords[2].startIndex, offsetBy: 1)]) ?? 0

        if currentYear < year {
            return false
        } else if currentYear == year && currentMonth < month {
            return false
        } else if currentYear == year && currentMonth == month && currentDay <= day + 1 {
            return false
        }

        return true
    }
}

extension EventsPresenter: EventsViewOutput {
    func didLoadView() {
        DataService.shared.getSchedule { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            guard let data = result.data,
                  let self = self else {
                return
            }

            self.model = Model(events: data.filter { rawModel in
                return !self.isFromPast(date: rawModel.datetime)
            }.map { rawModel in
                return self.transform(rawModel: rawModel)
            })

            self.view?.reloadData()
        }
    }

    func didSelectEvent(index: Int) {
        guard let event = model?.events[index] else {
            return
        }

        router?.showBoutsScreen(title: event.name,
                                eventId: event.id,
                                userId: UserDefaultsService.shared.getUserId())
    }

    func getEventsCount() -> Int {
        return model?.events.count ?? 0
    }

    func viewModel(index: Int) -> EventViewModel? {
        return model?.events[index]
    }
}
