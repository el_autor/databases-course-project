//
//  EventsRouter.swift
//  BoutMaker
//
//  Created by Vlad on 27.03.2021.
//

import UIKit

final class EventsRouter {
    weak var viewController: UIViewController?
}

extension EventsRouter: EventsRouterInput {
    func showBoutsScreen(title: String, eventId: Int, userId: Int) {
        let container = BoutsContainer.assemble(with: BoutsContext(title: title,
                                                                   eventId: eventId,
                                                                   userId: userId))

        container.viewController.modalPresentationStyle = .fullScreen
        viewController?.navigationController?.pushViewController(container.viewController,
                                                                 animated: true)
    }
}
