//
//  RatingProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 29.03.2021.
//

protocol RatingViewInput: AnyObject {
    func showAlert(title: String, message: String)

    func reloadData()
}

protocol RatingViewOutput: AnyObject {
    func didLoadView()

    func getUsersCount() -> Int

    func viewModel(index: Int) -> RatingViewModel?
}

protocol RatingRouterInput: AnyObject {}
