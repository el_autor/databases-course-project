//
//  RatingPresenter.swift
//  BoutMaker
//
//  Created by Vlad on 29.03.2021.
//

struct RatingViewModel {
    let index: String

    let username: String

    let points: String
}

final class RatingPresenter {
    private struct Model {
        let users: [RatingViewModel]
    }

    private var model: Model?

    weak var view: RatingViewInput?

    var router: RatingRouterInput?

    init() {}
}

extension RatingPresenter: RatingViewOutput {
    func didLoadView() {
        if let role = UserDefaultsService.shared.getUserRole(),
           role == "fan" {
            DataService.shared.getLeaderboard { [weak self] result in
                guard result.error == nil else {
                    guard let networkError = result.error else {
                        return
                    }

                    switch networkError {
                    case .networkNotReachable:
                        self?.view?.showAlert(title: "Oops!", message: "No connection")
                    default:
                        self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                    }

                    return
                }

                guard let data = result.data,
                      let self = self else {
                    return
                }

                var index = 0

                self.model = Model(users: data.map { user in
                    index += 1
                    return RatingViewModel(index: String(index),
                                         username: user.username, points: "\(user.points) pts")
                })

                self.view?.reloadData()
            }
        } else {
            DataService.shared.getPopularFighters { [weak self] result in
                guard result.error == nil else {
                    guard let networkError = result.error else {
                        return
                    }

                    switch networkError {
                    case .networkNotReachable:
                        self?.view?.showAlert(title: "Oops!", message: "No connection")
                    default:
                        self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                    }

                    return
                }

                guard let data = result.data,
                      let self = self else {
                    return
                }

                var index = 0

                self.model = Model(users: data.map { fighter in
                    index += 1
                    return RatingViewModel(index: String(index),
                                           username: fighter.name, points: "\(fighter.fans) fns")
                })

                self.view?.reloadData()
            }
        }
    }

    func getUsersCount() -> Int {
        return model?.users.count ?? 0
    }

    func viewModel(index: Int) -> RatingViewModel? {
        return model?.users[index]
    }
}
