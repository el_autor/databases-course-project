//
//  ContainerProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 29.03.2021.
//

import UIKit

final class RatingContainer {
    let viewController: UIViewController

    static func assemble(with context: RatingContext) -> RatingContainer {
        let viewController = RatingViewController()
        let presenter = RatingPresenter()
        let router = RatingRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return RatingContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct RatingContext {}
