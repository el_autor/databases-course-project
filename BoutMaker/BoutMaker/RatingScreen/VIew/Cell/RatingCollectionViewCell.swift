//
//  File.swift
//  BoutMaker
//
//  Created by Vlad on 29.03.2021.
//

import UIKit
import PinLayout

final class RatingCollectionViewCell: UICollectionViewCell {

    static let cellIdentifier: String = "ratingCell"

    // MARK: UI elements

    private weak var numberLabel: UILabel!

    private weak var usernameLabel: UILabel!

    private weak var pointsLabel: UILabel!

    // MARK: Setup

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupSubviews()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutNumberLabel()
        layoutUsernameLabel()
        layoutPointsLabel()
    }

    required init?(coder: NSCoder) {
        return nil
    }

    private func setupView() {
        layer.cornerRadius = 15
        backgroundColor = UIColor.mainColor
    }

    private func setupSubviews() {
        setupNumberLabel()
        setupUsernameLabel()
        setupPointsLabel()
    }

    public func configure(model: RatingViewModel) {
        numberLabel.text = model.index
        usernameLabel.text = model.username
        pointsLabel.text = model.points
    }

    private func setupNumberLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Bold",
                                        fontSize: 20,
                                        textColor: .white,
                                        textAlignment: .left,
                                        text: String())

        addSubview(label)
        numberLabel = label
    }

    private func setupUsernameLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        textAlignment: .left,
                                        text: String())

        addSubview(label)
        usernameLabel = label
    }

    private func setupPointsLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 22,
                                        textColor: .white,
                                        textAlignment: .right,
                                        text: String())

        addSubview(label)
        pointsLabel = label
    }

    // MARK: Layout

    private func layoutNumberLabel() {
        numberLabel.pin
            .top()
            .height(50%)
            .left(5%)
            .width(45%)
    }

    private func layoutUsernameLabel() {
        usernameLabel.pin
            .below(of: numberLabel)
            .marginTop(0)
            .height(50%)
            .left(5%)
            .width(45%)
    }

    private func layoutPointsLabel() {
        pointsLabel.pin
            .vCenter()
            .height(100%)
            .right(5%)
            .width(45%)
    }
}
