//
//  RatingCollectionViewHeader.swift
//  BoutMaker
//
//  Created by Vlad on 29.03.2021.
//

import UIKit
import PinLayout

final class RatingCollectionHeaderView: UICollectionReusableView {

    private weak var titleLabel: UILabel!

    static let reuseIdentifier = "ratingHeader"

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupSubviews()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutTitleLabel()
    }

    required init?(coder: NSCoder) {
        return nil
    }

    // MARK: Setup

    private func setupView() {

    }

    private func setupSubviews() {
        setupTitleLabel()
    }

    private func resolveHeaderText() -> String {
        if let role = UserDefaultsService.shared.getUserRole(),
           role == "fan" {
            return "Leaderboard"
        }

        return "Popular fighters"
    }

    private func setupTitleLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 25,
                                        textColor: .white,
                                        text: resolveHeaderText())

        addSubview(label)
        titleLabel = label
    }

    // MARK: Layout

    private func layoutTitleLabel() {
        titleLabel.pin
            .width(100%)
            .height(100%)
    }
}
