//
//  RatingViewController.swift
//  BoutMaker
//
//  Created by Vlad on 29.03.2021.
//

import UIKit
import PinLayout

final class RatingViewController: UIViewController {
    var output: RatingViewOutput?

    private weak var leaderboardCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutLeaderboardCollectionView()
    }

    // MARK: Setup

    private func setupView() {
        view.backgroundColor = UIColor.supportColor
    }

    private func setupSubviews() {
        setupLeaderboardCollectionView()
    }

    private func setupLeaderboardCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()

        flowLayout.itemSize = CGSize(width: 90%.of(UIScreen.main.bounds.width),
                                     height: 10%.of(UIScreen.main.bounds.height))

        let collectionView = UICollectionView(frame: .null,
                                              collectionViewLayout: flowLayout)

        collectionView.backgroundColor = UIColor.supportColor
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(RatingCollectionViewCell.self,
                                forCellWithReuseIdentifier: RatingCollectionViewCell.cellIdentifier)
        collectionView.register(RatingCollectionHeaderView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: RatingCollectionHeaderView.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(didRequestRefresh), for: .valueChanged)

        collectionView.refreshControl = refreshControl

        view.addSubview(collectionView)
        leaderboardCollectionView = collectionView
    }

    // MARK: Layout

    private func layoutLeaderboardCollectionView() {
        leaderboardCollectionView.pin
            .top()
            .hCenter()
            .width(90%)
            .bottom()
    }

    // MARK: Actions

    @objc
    private func didRequestRefresh() {
        output?.didLoadView()
    }
}

extension RatingViewController: UICollectionViewDelegate {

}

extension RatingViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 90%.of(UIScreen.main.bounds.width),
                      height: 7%.of(UIScreen.main.bounds.height))
    }
}

extension RatingViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: RatingCollectionHeaderView.reuseIdentifier,
                                                                             for: indexPath)

            return headerView
        }

        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getUsersCount() ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RatingCollectionViewCell.cellIdentifier, for: indexPath) as? RatingCollectionViewCell else {
            return UICollectionViewCell()
        }

        guard let viewModel = output?.viewModel(index: indexPath.row) else {
            return cell
        }

        cell.configure(model: viewModel)

        return cell
    }
}

extension RatingViewController: RatingViewInput {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    func reloadData() {
        leaderboardCollectionView.refreshControl?.endRefreshing()
        leaderboardCollectionView.reloadData()
    }
}
