import UIKit

final class LoginRouter {
    weak var viewController: UIViewController?
}

extension LoginRouter: LoginRouterInput {
    func showRegisterScreen() {
        let container = RegisterContainer.assemble(with: RegisterContext())

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.navigationController?.pushViewController(container.viewController,
                                                                 animated: true)
    }

    func showMainTabBar() {
        let container = MainTabBarContainer.assemble()

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.navigationController?.pushViewController(container.viewController,
                                                                 animated: true)
    }

    func showChooseRoleScreen(username: String) {
        let container = ChooseRoleContainer.assemble(with: ChooseRoleContext(username: username))

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.navigationController?.pushViewController(container.viewController,
                                                                 animated: true)
    }
}
