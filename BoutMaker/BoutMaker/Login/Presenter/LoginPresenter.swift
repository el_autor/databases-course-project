import Foundation

final class LoginPresenter {
	weak var view: LoginViewInput?

	private let router: LoginRouterInput

    init(router: LoginRouterInput) {
        self.router = router
    }
}

extension LoginPresenter: LoginViewOutput {
    func didTapSignUpLabel() {
        router.showRegisterScreen()
    }

    func didTapLogInButton() {
        guard let credentials = view?.getUserCredentials() else {
            return
        }

        guard let username = (credentials["username"] ?? ""),
              !username.isEmpty else {
            view?.showAlert(title: "Oops!", message: "Input username")
            return
        }

        guard let password = (credentials["password"] ?? ""),
              !password.isEmpty else {
            view?.showAlert(title: "Oops!", message: "Input password")
            return
        }

        AuthService.shared.login(username: username,
                                 password: password) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                case .userNotExist:
                    self?.view?.showAlert(title: "Oops!", message: "User not exist")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            guard let data = result.data else {
                return
            }

            UserDefaultsService.shared.authorize(username: username,
                                                 password: password,
                                                 userId: data.userId)

            if data.role == "default" {
                self?.router.showChooseRoleScreen(username: username)
            } else {
                UserDefaultsService.shared.setUserRole(role: data.role)
                self?.router.showMainTabBar()
            }
        }
    }

    func didTapView() {
        view?.disableKeyboard()
    }
}
