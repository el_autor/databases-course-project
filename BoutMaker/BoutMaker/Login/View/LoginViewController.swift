import UIKit
import PinLayout

final class LoginViewController: UIViewController {
	var output: LoginViewOutput?

    // MARK: UI elements

    private weak var greetingLabel: UILabel!

    private weak var downSubview: UIView!

    private weak var usernameTextField: UITextField!

    private weak var passwordTextField: UITextField!

    private weak var logInButton: UIButton!

    private weak var noAccountLabel: UILabel!

    private weak var signUpLabel: UILabel!

    private weak var signUpUnderscoreView: UIView!

	override func viewDidLoad() {
		super.viewDidLoad()

        setupView()
        setupSubviews()
	}

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutGreetingLabel()
        layoutDownSubview()
        layoutUsernameTextField()
        layoutPasswordTextField()
        layoutLogInButton()
        layoutNoAccountLabel()
        layoutSignUpLabel()
        layoutSignUpUnderscoreView()
    }

    private func setupView() {
        view.backgroundColor = UIColor.supportColor

        let tapRecognizer = UITapGestureRecognizer(target: self,
                                                           action: #selector(didTapView))

        tapRecognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapRecognizer)
    }

    private func setupSubviews() {
        setupGreetingLabel()
        setupDownSubview()
        setupUsernameTextField()
        setupPasswordTextField()
        setupLogInButton()
        setupNoAccountLabel()
        setupSignUpLabel()
        setupSignUpUnderscoreView()
    }

    private func setupGreetingLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 30,
                                        textColor: .white,
                                        text: "Welcome!")

        view.addSubview(label)
        greetingLabel = label
    }

    private func setupDownSubview() {
        let subview = UIView()

        view.addSubview(subview)
        downSubview = subview

        subview.backgroundColor = UIColor.mainColor
    }

    private func setupUsernameTextField() {
        let textField = UITextField.customTextField(placeholder: "Username",
                                                    fontsize: 14,
                                                    fontColor: .white,
                                                    backgroundColor: UIColor.supportColor)

        downSubview.addSubview(textField)
        usernameTextField = textField
    }

    private func setupPasswordTextField() {
        let textField = UITextField.customTextField(placeholder: "Password",
                                                    fontsize: 14,
                                                    fontColor: .white,
                                                    backgroundColor: UIColor.supportColor,
                                                    isSecure: true)

        downSubview.addSubview(textField)
        passwordTextField = textField
    }

    private func setupLogInButton() {
        let button = UIButton.customButton(title: "Log In",
                                           backgroundColor: UIColor.supportColor,
                                           cornerRadius: 10,
                                           fontName: "DMSans-Bold",
                                           fontSize: 16,
                                           fontColor: .white)

        downSubview.addSubview(button)
        logInButton = button
        logInButton.addTarget(self,
                              action: #selector(didTapLogInButton),
                              for: .touchUpInside)
    }

    private func setupNoAccountLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 14,
                                        textColor: .white,
                                        textAlignment: .right,
                                        text: "Don't have an account?")

        downSubview.addSubview(label)
        noAccountLabel = label
    }

    private func setupSignUpLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 14,
                                        textColor: .white,
                                        textAlignment: .left,
                                        text: "Sign Up")

        downSubview.addSubview(label)
        signUpLabel = label

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTapSignUpLabel))

        recognizer.numberOfTapsRequired = 1
        label.addGestureRecognizer(recognizer)
        label.isUserInteractionEnabled = true
    }

    private func setupSignUpUnderscoreView() {
        let subview = UIView()

        subview.backgroundColor = .white

        downSubview.addSubview(subview)
        signUpUnderscoreView = subview
    }

    // MARK: Layout

    private func layoutGreetingLabel() {
        greetingLabel.pin
            .top(13%)
            .hCenter()
            .width(100%)
            .height(5%)
    }

    private func layoutDownSubview() {
        downSubview.pin
            .top()
            .hCenter()
            .width(100%)
            .bottom()

        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.downSubview.pin
                .top(26%)
                .hCenter()
                .width(100%)
                .bottom()

            self?.downSubview.layer.cornerRadius = 40
            self?.downSubview.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }

    private func layoutUsernameTextField() {
        usernameTextField.pin
            .top(6%)
            .width(90%)
            .hCenter()
            .height(8%)
    }

    private func layoutPasswordTextField() {
        passwordTextField.pin
            .below(of: usernameTextField)
            .marginTop(4%)
            .width(90%)
            .hCenter()
            .height(8%)
    }

    private func layoutLogInButton() {
        logInButton.pin
            .below(of: passwordTextField)
            .marginTop(10%)
            .width(90%)
            .hCenter()
            .height(8%)
    }

    private func layoutNoAccountLabel() {
        noAccountLabel.pin
            .below(of: logInButton)
            .marginTop(3%)
            .width(60%)
            .left()
            .height(3%)
    }

    private func layoutSignUpLabel() {
        signUpLabel.pin
            .below(of: logInButton)
            .marginTop(3%)
            .width(39%)
            .right()
            .height(3%)
    }

    private func layoutSignUpUnderscoreView() {
        signUpUnderscoreView.pin
            .below(of: signUpLabel)
            .height(1)
            .left(61%)
            .width(50)
    }

    @objc
    private func didTapSignUpLabel() {
        output?.didTapSignUpLabel()
    }

    @objc
    private func didTapLogInButton() {
        output?.didTapLogInButton()
    }

    @objc
    private func didTapView() {
        output?.didTapView()
    }
}

extension LoginViewController: LoginViewInput {
    func getUserCredentials() -> [String: String?] {
        var credentials: [String: String?] = [:]

        credentials["username"] = usernameTextField.text
        credentials["password"] = passwordTextField.text

        return credentials
    }

    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    func disableKeyboard() {
        view.endEditing(true)
    }
}
