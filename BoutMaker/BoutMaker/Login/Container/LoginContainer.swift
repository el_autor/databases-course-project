import UIKit

final class LoginContainer {
	let viewController: UIViewController
	private(set) weak var router: LoginRouterInput!

	class func assemble(with context: LoginContext) -> LoginContainer {
        let router = LoginRouter()
        let presenter = LoginPresenter(router: router)
        let viewController = LoginViewController()

        viewController.output = presenter
        presenter.view = viewController
        router.viewController = viewController

        return LoginContainer(view: viewController, router: router)
	}

    private init(view: UIViewController, router: LoginRouterInput) {
		self.viewController = view
		self.router = router
	}
}

struct LoginContext {
}
