import Foundation

protocol LoginViewInput: AnyObject {
    func getUserCredentials() -> [String: String?]

    func showAlert(title: String, message: String)

    func disableKeyboard()
}

protocol LoginViewOutput: AnyObject {
    func didTapSignUpLabel()

    func didTapLogInButton()

    func didTapView()
}

protocol LoginRouterInput: AnyObject {
    func showRegisterScreen()

    func showMainTabBar()

    func showChooseRoleScreen(username: String)
}
