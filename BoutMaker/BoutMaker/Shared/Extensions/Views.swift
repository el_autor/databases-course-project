//
//  Views.swift
//  BoutMaker
//
//  Created by Vlad on 21.03.2021.
//

import UIKit

extension UILabel {
    static func customLabel(fontName: String,
                            fontSize: CGFloat,
                            textColor: UIColor,
                            textAlignment: NSTextAlignment = .center,
                            text: String) -> UILabel {
        let label = UILabel()

        label.font = UIFont(name: fontName, size: fontSize)
        label.textColor = textColor
        label.textAlignment = textAlignment
        label.text = text
        label.adjustsFontSizeToFitWidth = true

        return label
    }
}

extension UITextField {
    static func customTextField(placeholder: String,
                                fontsize: CGFloat,
                                fontColor: UIColor,
                                backgroundColor: UIColor,
                                isSecure: Bool = false) -> UITextField {
        let textField = UITextField()
        textField.attributedPlaceholder = NSAttributedString(string: String(repeating: " ", count: 2) + placeholder,
                                                                  attributes: [NSAttributedString.Key.foregroundColor: fontColor])

        textField.clipsToBounds = true
        textField.textColor = fontColor
        textField.tintColor = fontColor
        textField.layer.cornerRadius = 10
        textField.backgroundColor = backgroundColor
        textField.font = UIFont(name: "DMSans-Regular", size: fontsize)

        textField.leftView = UIView(frame: CGRect(x: .zero,
                                                      y: .zero,
                                                      width: 10,
                                                      height: .zero))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: .zero,
                                                       y: .zero,
                                                       width: 10,
                                                       height: .zero))
        textField.rightViewMode = .unlessEditing

        textField.clearButtonMode = .whileEditing
        textField.autocorrectionType = .no
        textField.isSecureTextEntry = isSecure

        return textField
    }
}

extension UIButton {
    static func customButton(title: String,
                             backgroundColor: UIColor,
                             cornerRadius: CGFloat,
                             fontName: String,
                             fontSize: CGFloat,
                             fontColor: UIColor) -> UIButton {
        let button = UIButton()

        button.backgroundColor = backgroundColor
        button.layer.cornerRadius = cornerRadius
        button.titleLabel?.font = UIFont(name: fontName, size: fontSize)
        button.setTitleColor(fontColor, for: .normal)
        button.setTitle(title, for: .normal)

        return button
    }
}
