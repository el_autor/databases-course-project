import UIKit

extension UIColor {
    static var mainColor: UIColor {
        return UIColor(red: 255 / 255,
                       green: 165 / 255,
                       blue: 0,
                       alpha: 1)
    }

    static var supportColor: UIColor {
        let palette: CGFloat = 163 / 255

        return UIColor(red: palette,
                       green: palette,
                       blue: palette,
                       alpha: 1)
    }
}
