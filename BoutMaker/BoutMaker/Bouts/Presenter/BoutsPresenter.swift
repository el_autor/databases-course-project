//
//  BoutsPresenter.swift
//  BoutMaker
//
//  Created by Vladislav Krivozubov on 18.04.2021.
//

import Foundation

struct BoutViewModel {
    struct FighterModel {
        let id: Int

        let firstname: String?

        let lastname: String?

        let record: String?
    }

    var id: Int

    var winnerId: Int?

    let weightCategory: String

    let firstFighter: FighterModel?

    let secondFighter: FighterModel?

    let isFinished: Bool

    let establishedWinnerId: Int?

    let pointsAreCollected: Bool
}

final class BoutsPresenter {
    weak var view: BoutsViewInput?

    var router: BoutsRouterInput?

    private var title: String

    private var eventId: Int

    private var userId: Int

    private var boutsRaw: [EventFullInfoRaw.BoutRaw] = []

    private var savedResults: [BoutSavedResultRaw] = []

    private var bouts: [BoutViewModel] = []

    init(title: String, eventId: Int, userId: Int) {
        self.title = title
        self.eventId = eventId
        self.userId = userId
    }

    private func buildRecordString(fighter: EventFullInfoRaw.BoutRaw.FighterRaw?) -> String? {
        guard let fighter = fighter else {
            return nil
        }

        return "\(fighter.wins)-\(fighter.losses)-\(fighter.draws)"
    }

    private func isFinished(boutRaw: EventFullInfoRaw.BoutRaw) -> Bool {
        return (boutRaw.status ?? String()) == "Final"
    }

    private func getWinnerId(boutRaw: EventFullInfoRaw.BoutRaw, savedModels: [BoutSavedResultRaw]) -> Int? {
        for model in savedModels where model.fightId == boutRaw.id {
            return model.winnerId
        }

        return nil
    }

    private func arePointsCollected(boutRaw: EventFullInfoRaw.BoutRaw, savedModels: [BoutSavedResultRaw]) -> Bool {
        for model in savedModels where model.fightId == boutRaw.id {
            return model.pointsAreCollected
        }

        return false
    }

    private func transform(rawModels: [EventFullInfoRaw.BoutRaw], savedModels: [BoutSavedResultRaw]) -> [BoutViewModel] {
        return rawModels.map { bout in
            var firstFighter: BoutViewModel.FighterModel?
            var secondFighter: BoutViewModel.FighterModel?

            if bout.fighters.count == 1 {
                firstFighter = .init(id: bout.fighters[0].id, firstname: bout.fighters.first?.firstname,
                                     lastname: bout.fighters.first?.lastname,
                                     record: buildRecordString(fighter: bout.fighters.first))
            } else if bout.fighters.count == 2 {
                firstFighter = .init(id: bout.fighters[0].id, firstname: bout.fighters.first?.firstname,
                                     lastname: bout.fighters.first?.lastname,
                                     record: buildRecordString(fighter: bout.fighters.first))

                secondFighter = .init(id: bout.fighters[1].id, firstname: bout.fighters.last?.firstname,
                                     lastname: bout.fighters.last?.lastname,
                                     record: buildRecordString(fighter: bout.fighters.last))
            }

            return BoutViewModel(id: bout.id,
                                 winnerId: getWinnerId(boutRaw: bout,
                                                       savedModels: savedModels),
                                 weightCategory: bout.weightCategory ?? "Unknown",
                                 firstFighter: firstFighter,
                                 secondFighter: secondFighter,
                                 isFinished: isFinished(boutRaw: bout),
                                 establishedWinnerId: bout.winnerId,
                                 pointsAreCollected: arePointsCollected(boutRaw: bout,
                                                                        savedModels: savedModels))
        }.filter { bout in
            if bout.firstFighter == nil && bout.secondFighter == nil {
                return false
            }

            return true
        }
    }
}

extension BoutsPresenter: BoutsViewOutput {
    func didTapBackGoBackImageView() {
        router?.goBack()
    }

    func didTapLeftFighter(index: Int) {
        guard let firstFighter = bouts[index].firstFighter,
              let secondFighter = bouts[index].secondFighter else {
            return
        }

        let cachedWinnerId = bouts[index].winnerId

        if let id = bouts[index].winnerId,
           id == firstFighter.id {
            bouts[index].winnerId = nil
        } else {
            bouts[index].winnerId = firstFighter.id
        }

        // view?.reloadData(index: index)

        DataService.shared.setFightResult(userId: userId,
                                          fighterIdA: firstFighter.id,
                                          fighterIdB: secondFighter.id,
                                          winnerId: bouts[index].winnerId,
                                          fightId: bouts[index].id,
                                          eventId: eventId) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    self?.bouts[index].winnerId = cachedWinnerId
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                    return
                }

                self?.bouts[index].winnerId = cachedWinnerId

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            self?.view?.reloadData(index: index)
        }
    }

    func didTapRightFighter(index: Int) {
        guard let firstFighter = bouts[index].firstFighter,
              let secondFighter = bouts[index].secondFighter else {
            return
        }

        let cachedWinnerId = bouts[index].winnerId

        if let id = bouts[index].winnerId,
           id == secondFighter.id {
            bouts[index].winnerId = nil
        } else {
            bouts[index].winnerId = secondFighter.id
        }

        // view?.reloadData(index: index)

        DataService.shared.setFightResult(userId: userId,
                                          fighterIdA: firstFighter.id,
                                          fighterIdB: secondFighter.id,
                                          winnerId: bouts[index].winnerId,
                                          fightId: bouts[index].id,
                                          eventId: eventId) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    self?.bouts[index].winnerId = cachedWinnerId
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                    return
                }

                self?.bouts[index].winnerId = cachedWinnerId

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            self?.view?.reloadData(index: index)
        }
    }

    func didLoadView() {
        let dispatchGroup = DispatchGroup()

        dispatchGroup.enter()
        DataService.shared.getEventInfo(eventId: eventId) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            guard let data = result.data,
                  let self = self else {
                return
            }

            self.boutsRaw = data.bouts
            dispatchGroup.leave()
        }

        dispatchGroup.enter()
        DataService.shared.getSavedEventInfo(eventId: eventId, userId: userId) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            guard let data = result.data,
                  let self = self else {
                return
            }

            self.savedResults = data
            dispatchGroup.leave()
        }

        dispatchGroup.notify(queue: .main) {
            self.bouts = self.transform(rawModels: self.boutsRaw,
                                   savedModels: self.savedResults)
            self.view?.reloadData(index: nil)
        }
    }

    func didLoadHeaderView() {
        view?.setTitleText(text: title)
    }

    func getBoutsCount() -> Int {
        return bouts.count
    }

    func viewModel(index: Int) -> BoutViewModel {
        return bouts[index]
    }

    func didTapCollectButton(index: Int) {
        DataService.shared.collectPoints(userId: userId, fightId: bouts[index].id) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }
        }
    }
}
