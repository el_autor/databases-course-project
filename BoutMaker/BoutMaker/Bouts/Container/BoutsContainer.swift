//
//  BoutsContainer.swift
//  BoutMaker
//
//  Created by Vladislav Krivozubov on 18.04.2021.
//

import UIKit

final class BoutsContainer {
    let viewController: UIViewController

    static func assemble(with context: BoutsContext) -> BoutsContainer {
        let viewController = BoutsViewController()
        let presenter = BoutsPresenter(title: context.title,
                                       eventId: context.eventId,
                                       userId: context.userId)
        let router = BoutsRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return BoutsContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct BoutsContext {
    let title: String

    let eventId: Int

    let userId: Int
}
