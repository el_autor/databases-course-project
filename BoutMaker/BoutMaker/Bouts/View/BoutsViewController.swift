//
//  BoutsViewController.swift
//  BoutMaker
//
//  Created by Vladislav Krivozubov on 18.04.2021.
//

import UIKit
import PinLayout

final class BoutsViewController: UIViewController {
    var output: BoutsViewOutput?

    // MARK: UI Elements

    private var boutsCollectionView: UICollectionView!

    private var header: BoutsCollectionHeaderView! {
        didSet {
            output?.didLoadHeaderView()
        }
    }

    // MARK: Setup

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    private func setupView() {
        view.backgroundColor = UIColor.supportColor
    }

    private func setupSubviews() {
        setupBoutsCollectionView()
    }

    private func setupBoutsCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()

        flowLayout.itemSize = CGSize(width: 90%.of(UIScreen.main.bounds.width),
                                     height: 25%.of(UIScreen.main.bounds.height))

        let collectionView = UICollectionView(frame: .null,
                                              collectionViewLayout: flowLayout)

        collectionView.backgroundColor = UIColor.supportColor
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(BoutsCollectionViewCell.self,
                                forCellWithReuseIdentifier: BoutsCollectionViewCell.cellIdentifier)
        collectionView.register(BoutsCollectionHeaderView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: BoutsCollectionHeaderView.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self

        let refreshControl = UIRefreshControl()

        refreshControl.addTarget(self,
                                 action: #selector(didSwipe), for: .valueChanged)
        collectionView.refreshControl = refreshControl

        view.addSubview(collectionView)
        boutsCollectionView = collectionView
    }

    // MARK: Layout

    override func viewDidLayoutSubviews() {
        layoutBoutsCollectionView()
    }

    private func layoutBoutsCollectionView() {
        boutsCollectionView.pin
            .all()
    }

    // MARK: Actions

    @objc
    private func didSwipe() {
        output?.didLoadView()
    }
}

extension BoutsViewController: UICollectionViewDelegate {}

extension BoutsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 90%.of(UIScreen.main.bounds.width),
                      height: 7%.of(UIScreen.main.bounds.height))
    }
}

extension BoutsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: BoutsCollectionHeaderView.reuseIdentifier,
                                                                             for: indexPath)

            guard let header = headerView as? BoutsCollectionHeaderView else {
                return headerView
            }

            header.output = output
            self.header = header

            return headerView
        }

        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let output = output else {
            return 0
        }

        return output.getBoutsCount()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BoutsCollectionViewCell.cellIdentifier, for: indexPath) as? BoutsCollectionViewCell else {
            return UICollectionViewCell()
        }

        guard let viewModel = output?.viewModel(index: indexPath.row) else {
            return cell
        }

        cell.output = output
        cell.index = indexPath.row
        cell.configure(model: viewModel)
        cell.enableUserInteraction()

        return cell
    }
}

extension BoutsViewController: BoutsViewInput {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    func setTitleText(text: String) {
        header.titleLabel.text = text
    }

    func reloadData(index: Int?) {
        if let index = index {
            UIView.performWithoutAnimation {
                boutsCollectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
            }
        } else {
            boutsCollectionView.refreshControl?.endRefreshing()
            boutsCollectionView.reloadData()
        }
    }
}
