//
//  BoutsCollectionHeaderView.swift
//  BoutMaker
//
//  Created by Vladislav Krivozubov on 18.04.2021.
//

import UIKit
import PinLayout

final class BoutsCollectionHeaderView: UICollectionReusableView {

    static let reuseIdentifier = "boutsHeader"

    weak var output: BoutsViewOutput?

    // MARK: UI Elements

    var titleLabel: UILabel!

    private var goBackImageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupSubviews()
    }

    required init?(coder: NSCoder) {
        return nil
    }

    // MARK: Setup

    private func setupView() {

    }

    private func setupSubviews() {
        setupTitleLabel()
        setupGoBackImageView()
    }

    private func setupTitleLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 25,
                                        textColor: .white,
                                        text: "UFC 261")

        addSubview(label)
        titleLabel = label
    }

    private func setupGoBackImageView() {
        let imageView = UIImageView()

        imageView.image = UIImage(systemName: "chevron.backward",
                                  withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit

        let tapRecognizer = UITapGestureRecognizer(target: self,
                                                   action: #selector(didTapGoBackImageView))
        tapRecognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tapRecognizer)
        imageView.isUserInteractionEnabled = true

        addSubview(imageView)
        goBackImageView = imageView
    }

    // MARK: Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutTitleLabel()
        layoutGoBackImageView()
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .width(70%)
            .height(45%)
            .hCenter()
            .vCenter()
    }

    private func layoutGoBackImageView() {
        goBackImageView.pin
            .top(30%)
            .bottom(30%)

        goBackImageView.pin
            .width(goBackImageView.bounds.height)
            .left(5%)
    }

    // MARK: Actions

    @objc
    private func didTapGoBackImageView() {
        output?.didTapBackGoBackImageView()
    }
}
