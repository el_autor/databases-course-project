//
//  BoutsCollectionViewCell.swift
//  BoutMaker
//
//  Created by Vladislav Krivozubov on 18.04.2021.
//

import UIKit
import PinLayout

final class MarkImageView: UIImageView {

    lazy var symbolImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(systemName: "checkmark", withConfiguration: UIImage.SymbolConfiguration(weight: .bold)))

        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    override init(image: UIImage?) {
        super.init(image: image)

        self.image = image

        addSubview(symbolImageView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        symbolImageView.pin
            .height(50%)
            .width(50%)
            .hCenter()
            .vCenter()
    }
}

final class FighterDescriptionView: UIView {

    lazy var markImageView: MarkImageView = {
        let imageView = MarkImageView(image: UIImage(systemName: "circle.fill"))

        imageView.tintColor = .systemGreen
        imageView.contentMode = .scaleAspectFit

        return imageView
    }()

    lazy var firstnameLabel: UILabel = {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: String())

        return label
    }()

    lazy var lastnameLabel: UILabel = {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: String())

        return label
    }()

    lazy var recordLabel: UILabel = {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: String())

        return label
    }()

    lazy var percentageLabel: UILabel = {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: String())

        label.isHidden = true
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)

        layer.cornerRadius = 10

        addSubview(markImageView)
        addSubview(firstnameLabel)
        addSubview(lastnameLabel)
        addSubview(recordLabel)
        addSubview(percentageLabel)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        markImageView.pin
            .width(30%)
            .top()

        markImageView.pin
            .height(markImageView.bounds.width)
            .hCenter()

        firstnameLabel.pin
            .width(100%)
            .height(12%)
            .below(of: markImageView)
            .marginTop(15%)
            .hCenter()

        lastnameLabel.pin
            .width(100%)
            .height(12%)
            .below(of: firstnameLabel)
            .marginTop(2%)
            .hCenter()

        recordLabel.pin
            .width(100%)
            .height(12%)
            .below(of: lastnameLabel)
            .marginTop(5%)
            .hCenter()

        percentageLabel.pin
            .width(100%)
            .height(12%)
            .below(of: recordLabel)
            .marginTop(5%)
            .hCenter()
    }
}

final class BoutsCollectionViewCell: UICollectionViewCell {

    static let cellIdentifier: String = "boutCell"

    var output: BoutsViewOutput?

    var index: Int?

    // MARK: UI elements

    private lazy var categoryLabel: UILabel = {
        return UILabel.customLabel(fontName: "DMSans-Medium",
                                   fontSize: 15,
                                   textColor: .white,
                                   text: String())
    }()

    private lazy var leftFighterView: FighterDescriptionView = {
        let view = FighterDescriptionView()

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTapLeftFighter))
        recognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(recognizer)
        view.isUserInteractionEnabled = true

        return view
    }()

    private lazy var separatorLabel: UILabel = {
        return UILabel.customLabel(fontName: "DMSans-Medium",
                                   fontSize: 15,
                                   textColor: .white,
                                   text: "vs")
    }()

    private lazy var rightFighterView: FighterDescriptionView = {
        let view = FighterDescriptionView()

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTapRightFighter))
        recognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(recognizer)
        view.isUserInteractionEnabled = true

        return view
    }()

    private lazy var getPointsButton: UIButton = {
        let button = UIButton.customButton(title: "Get 10 p.",
                                           backgroundColor: .supportColor,
                                           cornerRadius: 5,
                                           fontName: "DMSans-Bold",
                                           fontSize: 14,
                                           fontColor: .white)

        button.addTarget(self, action: #selector(didTapGetPointsButton), for: .touchUpInside)
        button.isHidden = true

        return button
    }()

    private lazy var pointsCollectedLabel: UILabel = {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: "Collected!")

        label.isHidden = true

        return label
    }()

    // MARK: Setup

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()

        addSubview(categoryLabel)
        addSubview(leftFighterView)
        addSubview(separatorLabel)
        addSubview(rightFighterView)
        addSubview(getPointsButton)
        addSubview(pointsCollectedLabel)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        categoryLabel.pin
            .height(7%)
            .width(35%)
            .hCenter()
            .top(10%)

        leftFighterView.pin
            .height(80%)
            .width(25%)
            .top(10%)
            .left(5%)

        separatorLabel.pin
            .width(10%)
            .height(10%)
            .hCenter()
            .vCenter()

        rightFighterView.pin
            .height(80%)
            .width(25%)
            .top(10%)
            .right(5%)

        getPointsButton.pin
            .height(15%)
            .width(30%)
            .below(of: separatorLabel)
            .marginTop(20%)
            .hCenter()

        pointsCollectedLabel.pin
            .height(15%)
            .width(30%)
            .below(of: separatorLabel)
            .marginTop(20%)
            .hCenter()
    }

    required init?(coder: NSCoder) {
        return nil
    }

    private func setupView() {
        backgroundColor = UIColor.mainColor
        layer.cornerRadius = 15
    }

    public func enableUserInteraction() {
        leftFighterView.isUserInteractionEnabled = true
        rightFighterView.isUserInteractionEnabled = true
    }

    public func configure(model: BoutViewModel) {
        categoryLabel.text = model.weightCategory

        UIView.animate(withDuration: 0.3) {
            self.leftFighterView.percentageLabel.isHidden = true
        }

        UIView.animate(withDuration: 0.3) {
            self.rightFighterView.percentageLabel.isHidden = true
        }

        if let firstFighter = model.firstFighter {
            leftFighterView.firstnameLabel.text = firstFighter.firstname
            leftFighterView.lastnameLabel.text = firstFighter.lastname
            leftFighterView.recordLabel.text = firstFighter.record

            if let role = UserDefaultsService.shared.getUserRole(),
               role == "matchmaker" {
                DataService.shared.getFightStats(fightId: model.id) { [weak self] result in
                    guard result.error == nil else {
                        return
                    }

                    guard let data = result.data else {
                        return
                    }

                    DispatchQueue.main.async { [weak self] in
                        self?.leftFighterView.percentageLabel.text = data.firstFighterPercentage + "%"
                        self?.rightFighterView.percentageLabel.text = data.secondFighterPercentage + "%"

                        UIView.animate(withDuration: 0.3) {
                            self?.leftFighterView.percentageLabel.isHidden = false
                        }

                        UIView.animate(withDuration: 0.3) {
                            self?.rightFighterView.percentageLabel.isHidden = false
                        }
                    }
                }
            }

            if model.isFinished {
                if !model.pointsAreCollected {
                    getPointsButton.isHidden = false
                    pointsCollectedLabel.isHidden = true
                } else {
                    pointsCollectedLabel.isHidden = false
                }

                if let predictedWinnerId = model.winnerId,
                   let establishedWinnerId = model.establishedWinnerId,
                   predictedWinnerId == establishedWinnerId,
                   predictedWinnerId == firstFighter.id {
                    leftFighterView.markImageView.tintColor = .systemBlue
                    leftFighterView.markImageView.symbolImageView.image = UIImage(systemName: "checkmark", withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
                    leftFighterView.markImageView.symbolImageView.tintColor = .white
                    leftFighterView.markImageView.isHidden = false
                    leftFighterView.backgroundColor = .systemGreen
                } else if let predictedWinnerId = model.winnerId,
                          let establishedWinnerId = model.establishedWinnerId,
                          predictedWinnerId != establishedWinnerId,
                          predictedWinnerId == firstFighter.id {
                    leftFighterView.markImageView.tintColor = .systemBlue
                    leftFighterView.markImageView.symbolImageView.image = UIImage(systemName: "checkmark", withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
                    leftFighterView.markImageView.symbolImageView.tintColor = .white
                    leftFighterView.markImageView.isHidden = false
                    getPointsButton.isHidden = true
                    leftFighterView.backgroundColor = .systemRed
                } else if let predictedWinnerId = model.winnerId,
                          let establishedWinnerId = model.establishedWinnerId,
                          predictedWinnerId != establishedWinnerId,
                          predictedWinnerId != firstFighter.id {
                    leftFighterView.markImageView.isHidden = true
                    getPointsButton.isHidden = true
                    leftFighterView.backgroundColor = .systemGreen
                } else if let predictedWinnerId = model.winnerId,
                          let establishedWinnerId = model.establishedWinnerId,
                          predictedWinnerId == establishedWinnerId,
                          predictedWinnerId != firstFighter.id {
                    leftFighterView.markImageView.isHidden = true
                    leftFighterView.backgroundColor = .systemRed
                } else if let establishedWinnerId = model.establishedWinnerId,
                          establishedWinnerId == firstFighter.id {
                    leftFighterView.markImageView.isHidden = true
                    leftFighterView.backgroundColor = .systemGreen
                    getPointsButton.isHidden = true
                } else {
                    leftFighterView.markImageView.isHidden = true
                    leftFighterView.backgroundColor = .systemRed
                    getPointsButton.isHidden = true
                }
            } else {
                getPointsButton.isHidden = true
                if let winnerId = model.winnerId,
                   let fighterId = model.firstFighter?.id {
                    if winnerId == fighterId {
                        leftFighterView.markImageView.tintColor = .systemBlue
                        leftFighterView.markImageView.symbolImageView.image = UIImage(systemName: "checkmark", withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
                        leftFighterView.markImageView.symbolImageView.tintColor = .white
                        leftFighterView.markImageView.isHidden = false
                    } else {
                        leftFighterView.markImageView.isHidden = true
                    }
                } else {
                    leftFighterView.markImageView.isHidden = true
                }
            }
        } else {
            leftFighterView.firstnameLabel.text = "Unknown"
        }

        if let secondFighter = model.secondFighter {
            rightFighterView.firstnameLabel.text = secondFighter.firstname
            rightFighterView.lastnameLabel.text = secondFighter.lastname
            rightFighterView.recordLabel.text = secondFighter.record

            if model.isFinished {
                if !model.pointsAreCollected {
                    getPointsButton.isHidden = false
                    pointsCollectedLabel.isHidden = true
                } else {
                    pointsCollectedLabel.isHidden = false
                }

                if let predictedWinnerId = model.winnerId,
                   let establishedWinnerId = model.establishedWinnerId,
                   predictedWinnerId == establishedWinnerId,
                   predictedWinnerId == secondFighter.id {
                    rightFighterView.markImageView.tintColor = .systemBlue
                    rightFighterView.markImageView.symbolImageView.image = UIImage(systemName: "checkmark", withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
                    rightFighterView.markImageView.symbolImageView.tintColor = .white
                    rightFighterView.markImageView.isHidden = false
                    rightFighterView.backgroundColor = .systemGreen
                } else if let predictedWinnerId = model.winnerId,
                          let establishedWinnerId = model.establishedWinnerId,
                          predictedWinnerId != establishedWinnerId,
                          predictedWinnerId == secondFighter.id {
                    rightFighterView.markImageView.tintColor = .systemBlue
                    rightFighterView.markImageView.symbolImageView.image = UIImage(systemName: "checkmark", withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
                    rightFighterView.markImageView.symbolImageView.tintColor = .white
                    rightFighterView.markImageView.isHidden = false
                    getPointsButton.isHidden = true
                    rightFighterView.backgroundColor = .systemRed
                } else if let predictedWinnerId = model.winnerId,
                          let establishedWinnerId = model.establishedWinnerId,
                          predictedWinnerId != establishedWinnerId,
                          predictedWinnerId != secondFighter.id {
                    rightFighterView.markImageView.isHidden = true
                    getPointsButton.isHidden = true
                    rightFighterView.backgroundColor = .systemGreen
                } else if let predictedWinnerId = model.winnerId,
                          let establishedWinnerId = model.establishedWinnerId,
                          predictedWinnerId == establishedWinnerId,
                          predictedWinnerId != secondFighter.id {
                    rightFighterView.markImageView.isHidden = true
                    rightFighterView.backgroundColor = .systemRed
                } else if let establishedWinnerId = model.establishedWinnerId,
                          establishedWinnerId == secondFighter.id {
                    rightFighterView.markImageView.isHidden = true
                    rightFighterView.backgroundColor = .systemGreen
                    getPointsButton.isHidden = true
                } else {
                    rightFighterView.markImageView.isHidden = true
                    rightFighterView.backgroundColor = .systemRed
                    getPointsButton.isHidden = true
                }
            } else {
                getPointsButton.isHidden = true
                if let winnerId = model.winnerId,
                   let fighterId = model.secondFighter?.id {
                    if winnerId == fighterId {
                        rightFighterView.markImageView.tintColor = .systemBlue
                        rightFighterView.markImageView.symbolImageView.image = UIImage(systemName: "checkmark", withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
                        rightFighterView.markImageView.symbolImageView.tintColor = .white
                        rightFighterView.markImageView.isHidden = false
                    } else {
                        rightFighterView.markImageView.isHidden = true
                    }
                } else {
                    rightFighterView.markImageView.isHidden = true
                }
            }
        } else {
            rightFighterView.firstnameLabel.text = "Unknown"
        }
    }

    // MARK: Actions

    @objc
    private func didTapGetPointsButton() {
        guard let index = index else {
            return
        }

        getPointsButton.isHidden = true
        pointsCollectedLabel.isHidden = false
        output?.didTapCollectButton(index: index)
    }

    @objc
    private func didTapLeftFighter() {
        guard let index = index else {
            return
        }

        leftFighterView.isUserInteractionEnabled = false
        rightFighterView.isUserInteractionEnabled = false
        output?.didTapLeftFighter(index: index)
    }

    @objc
    private func didTapRightFighter() {
        guard let index = index else {
            return
        }

        leftFighterView.isUserInteractionEnabled = false
        rightFighterView.isUserInteractionEnabled = false
        output?.didTapRightFighter(index: index)
    }
}
