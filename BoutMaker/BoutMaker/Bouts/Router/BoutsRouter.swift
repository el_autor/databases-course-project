//
//  BoutsRouter.swift
//  BoutMaker
//
//  Created by Vladislav Krivozubov on 18.04.2021.
//

import UIKit

final class BoutsRouter {
    weak var viewController: UIViewController?
}

extension BoutsRouter: BoutsRouterInput {
    func goBack() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
