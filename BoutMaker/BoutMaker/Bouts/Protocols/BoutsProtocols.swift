//
//  BoutsProtocols.swift
//  BoutMaker
//
//  Created by Vladislav Krivozubov on 18.04.2021.
//

import Foundation

protocol BoutsViewInput: AnyObject {
    func showAlert(title: String, message: String)

    func setTitleText(text: String)

    func reloadData(index: Int?)
}

protocol BoutsViewOutput: AnyObject {
    func didTapBackGoBackImageView()

    func didLoadView()

    func didLoadHeaderView()

    func getBoutsCount() -> Int

    func viewModel(index: Int) -> BoutViewModel

    func didTapLeftFighter(index: Int)

    func didTapRightFighter(index: Int)

    func didTapCollectButton(index: Int)
}

protocol BoutsRouterInput: AnyObject {
    func goBack()
}
