//
//  RegisterViewController.swift
//  BoutMaker
//
//  Created by Vlad on 21.03.2021.
//

import UIKit
import PinLayout

final class RegisterViewController: UIViewController {
    var output: RegisterViewOutput?

    // MARK: UI elements

    private weak var greetingLabel: UILabel!

    private weak var downSubview: UIView!

    private weak var usernameTextField: UITextField!

    private weak var passwordTextField: UITextField!

    private weak var repeatPasswordTextField: UITextField!

    private weak var signUpButton: UIButton!

    private weak var alreadyRegisteredLabel: UILabel!

    private weak var logInLabel: UILabel!

    private weak var logInUnderscoreView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()
    }

    private func setupView() {
        view.backgroundColor = UIColor.supportColor

        let tapRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(didTapView)
        )

        tapRecognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapRecognizer)
    }

    private func setupSubviews() {
        setupGreetingLabel()
        setupDownSubview()
        setupUsernameTextField()
        setupPasswordTextField()
        setupRepeatPasswordTextField()
        setupSignUpButton()
        setupAlreadyRegisteredLabel()
        setupLogInLabel()
        setupLogInUnderscoreView()
    }

    private func setupGreetingLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 30,
                                        textColor: .white,
                                        text: "Welcome!")

        view.addSubview(label)
        greetingLabel = label
    }

    private func setupDownSubview() {
        let subview = UIView()

        view.addSubview(subview)
        downSubview = subview

        subview.backgroundColor = UIColor.mainColor
        subview.layer.cornerRadius = 40
        subview.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }

    private func setupUsernameTextField() {
        let textField = UITextField.customTextField(placeholder: "Username",
                                                    fontsize: 14,
                                                    fontColor: .white,
                                                    backgroundColor: UIColor.supportColor)

        downSubview.addSubview(textField)
        usernameTextField = textField
    }

    private func setupPasswordTextField() {
        let textField = UITextField.customTextField(placeholder: "Password",
                                                    fontsize: 14,
                                                    fontColor: .white,
                                                    backgroundColor: UIColor.supportColor,
                                                    isSecure: true)

        downSubview.addSubview(textField)
        passwordTextField = textField
    }

    private func setupRepeatPasswordTextField() {
        let textField = UITextField.customTextField(placeholder: "Repeat Password",
                                                    fontsize: 14,
                                                    fontColor: .white,
                                                    backgroundColor: UIColor.supportColor,
                                                    isSecure: true)

        downSubview.addSubview(textField)
        repeatPasswordTextField = textField
    }

    private func setupSignUpButton() {
        let button = UIButton.customButton(title: "Sign Up",
                                           backgroundColor: UIColor.supportColor,
                                           cornerRadius: 10,
                                           fontName: "DMSans-Bold",
                                           fontSize: 16,
                                           fontColor: .white)

        downSubview.addSubview(button)
        signUpButton = button

        signUpButton.addTarget(self,
                               action: #selector(didTapSignUpButton), for: .touchUpInside)
    }

    private func setupAlreadyRegisteredLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 14,
                                        textColor: .white,
                                        textAlignment: .right,
                                        text: "Already registered?")

        downSubview.addSubview(label)
        alreadyRegisteredLabel = label
    }

    private func setupLogInLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 14,
                                        textColor: .white,
                                        textAlignment: .left,
                                        text: "Log In")

        downSubview.addSubview(label)
        logInLabel = label

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTapLogInLabel))

        recognizer.numberOfTapsRequired = 1
        label.addGestureRecognizer(recognizer)
        label.isUserInteractionEnabled = true

    }

    private func setupLogInUnderscoreView() {
        let subview = UIView()

        subview.backgroundColor = .white

        downSubview.addSubview(subview)
        logInUnderscoreView = subview
    }

    // MARK: Layout

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutGreetingLabel()
        layoutDownSubview()
        layoutUsernameTextField()
        layoutPasswordTextField()
        layoutRepeatPasswordTextField()
        layoutSignUpButton()
        layoutAlreadyRegisteredLabel()
        layoutLogInLabel()
        layoutLogInUnderscoreView()
    }

    private func layoutGreetingLabel() {
        greetingLabel.pin
            .top(13%)
            .hCenter()
            .width(100%)
            .height(5%)
    }

    private func layoutDownSubview() {
        downSubview.pin
            .top(26%)
            .hCenter()
            .width(100%)
            .bottom()
    }

    private func layoutUsernameTextField() {
        usernameTextField.pin
            .top(6%)
            .width(90%)
            .hCenter()
            .height(8%)
    }

    private func layoutPasswordTextField() {
        passwordTextField.pin
            .below(of: usernameTextField)
            .marginTop(4%)
            .width(90%)
            .hCenter()
            .height(8%)
    }

    private func layoutRepeatPasswordTextField() {
        repeatPasswordTextField.pin
            .below(of: passwordTextField)
            .marginTop(4%)
            .width(90%)
            .hCenter()
            .height(8%)
    }

    private func layoutSignUpButton() {
        signUpButton.pin
            .below(of: repeatPasswordTextField)
            .marginTop(10%)
            .width(90%)
            .hCenter()
            .height(8%)
    }

    private func layoutAlreadyRegisteredLabel() {
        alreadyRegisteredLabel.pin
            .below(of: signUpButton)
            .marginTop(3%)
            .width(60%)
            .left()
            .height(3%)
    }

    private func layoutLogInLabel() {
        logInLabel.pin
            .below(of: signUpButton)
            .marginTop(3%)
            .width(39%)
            .right()
            .height(3%)
    }

    private func layoutLogInUnderscoreView() {
        logInUnderscoreView.pin
            .below(of: logInLabel)
            .height(1)
            .left(61%)
            .width(40)
    }

    @objc
    private func didTapLogInLabel() {
        output?.didTapLogInLabel()
    }

    @objc
    private func didTapView() {
        output?.didTapView()
    }

    @objc
    private func didTapSignUpButton() {
        output?.didTapSignUpButton()
    }
}

extension RegisterViewController: RegisterViewInput {
    func disableKeyboard() {
        view.endEditing(true)
    }

    func getUserCredentials() -> [String: String?] {
        var credentials: [String: String?] = [:]

        credentials["username"] = usernameTextField.text
        credentials["password"] = passwordTextField.text
        credentials["repeatPassword"] = repeatPasswordTextField.text

        return credentials
    }

    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }
}
