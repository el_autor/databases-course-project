//
//  ContainerRouter.swift
//  BoutMaker
//
//  Created by Vlad on 21.03.2021.
//

import UIKit

final class RegisterContainer {
    let viewController: UIViewController

    static func assemble(with context: RegisterContext) -> RegisterContainer {
        let viewController = RegisterViewController()
        let presenter = RegisterPresenter()
        let router = RegisterRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return RegisterContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct RegisterContext { }
