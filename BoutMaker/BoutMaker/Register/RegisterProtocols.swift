//
//  RegisterProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 21.03.2021.
//

protocol RegisterViewInput: AnyObject {
    func disableKeyboard()

    func getUserCredentials() -> [String: String?]

    func showAlert(title: String, message: String)
}

protocol RegisterViewOutput: AnyObject {
    func didTapLogInLabel()

    func didTapView()

    func didTapSignUpButton()
}

protocol RegisterRouterInput: AnyObject {
    func showLogInScreen()

    func showChooseRoleScreen(username: String)
}
