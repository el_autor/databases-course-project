//
//  PresenterRouter.swift
//  BoutMaker
//
//  Created by Vlad on 21.03.2021.
//

final class RegisterPresenter {
    weak var view: RegisterViewInput?

    var router: RegisterRouterInput?
}

extension RegisterPresenter: RegisterViewOutput {
    func didTapLogInLabel() {
        router?.showLogInScreen()
    }

    func didTapView() {
        view?.disableKeyboard()
    }

    func didTapSignUpButton() {
        guard let credentials = view?.getUserCredentials() else {
            return
        }

        guard let username = (credentials["username"] ?? ""),
              !username.isEmpty else {
            view?.showAlert(title: "Oops!", message: "Please, input username")
            return
        }

        guard let password = (credentials["password"] ?? ""),
              !password.isEmpty else {
            view?.showAlert(title: "Oops!", message: "Please, input password")
            return
        }

        guard let repeatPassword = (credentials["repeatPassword"] ?? ""),
              !repeatPassword.isEmpty else {
            view?.showAlert(title: "Oops!", message: "Please, repeat password")
            return
        }

        guard password == repeatPassword else {
            view?.showAlert(title: "Oops!", message: "Passwords not equal")
            return
        }

        guard password.count > 5 else {
            view?.showAlert(title: "Oops!", message: "Password length must be > 5")
            return
        }

        AuthService.shared.register(username: username,
                                    password: password) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                case .userAlreadyExist:
                    self?.view?.showAlert(title: "Oops!", message: "User already exist")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            guard let data = result.data else {
                return
            }

            UserDefaultsService.shared.authorize(username: username,
                                                 password: password,
                                                 userId: data.userId)

            self?.router?.showChooseRoleScreen(username: username)
        }
    }
}
