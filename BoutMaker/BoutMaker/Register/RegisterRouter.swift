//
//  RegisterRouter.swift
//  BoutMaker
//
//  Created by Vlad on 21.03.2021.
//

import UIKit

final class RegisterRouter {
    weak var viewController: UIViewController?
}

extension RegisterRouter: RegisterRouterInput {
    func showLogInScreen() {
        viewController?.navigationController?.popViewController(animated: true)
    }

    func showChooseRoleScreen(username: String) {
        let container = ChooseRoleContainer.assemble(with: ChooseRoleContext(username: username))

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.navigationController?.pushViewController(container.viewController,
                                                                 animated: true)
    }
}
