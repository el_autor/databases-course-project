//
//  BestProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

protocol BestViewInput: AnyObject {
    func showAlert(title: String, message: String)

    func reloadData()
}

protocol BestViewOutput: AnyObject {
    func didTapAddImageView()

    func didLoadView()

    func getBestFightsCount() -> Int?

    func viewModel(index: Int) -> BestFightViewModel?

    func didDoubleTapCell(index: Int)
}

protocol BestRouterInput: AnyObject {
    func showAddBestScreen()
}
