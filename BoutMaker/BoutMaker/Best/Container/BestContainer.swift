//
//  BestContainer.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit

final class BestContainer {
    let viewController: UIViewController

    static func assemble(with context: BestContext) -> BestContainer {
        let viewController = BestViewController()
        let presenter = BestPresenter()
        let router = BestRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return BestContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct BestContext {}
