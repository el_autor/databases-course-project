//
//  BestCollectionViewHeader.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit
import PinLayout

final class BestCollectionHeaderView: UICollectionReusableView {

    static let reuseIdentifier = "bestHeader"

    weak var output: BestViewOutput?

    // MARK: UI Elements

    private weak var titleLabel: UILabel!

    private weak var addImageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupSubviews()
    }

    required init?(coder: NSCoder) {
        return nil
    }

    // MARK: Setup

    private func setupView() {

    }

    private func setupSubviews() {
        setupTitleLabel()
        setupAddImageView()
    }

    private func setupTitleLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 25,
                                        textColor: .white,
                                        text: "Best")

        addSubview(label)
        titleLabel = label
    }

    private func setupAddImageView() {
        let imageView = UIImageView()

        imageView.image = UIImage(systemName: "plus",
                                  withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit

        let tapRecognizer = UITapGestureRecognizer(target: self,
                                                   action: #selector(didTapAddImageView))
        tapRecognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tapRecognizer)
        imageView.isUserInteractionEnabled = true

        addSubview(imageView)
        addImageView = imageView
    }

    // MARK: Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutTitleLabel()
        layoutAddImageView()
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .top()
            .bottom()
            .width(70%)
            .hCenter()
    }

    private func layoutAddImageView() {
        addImageView.pin
            .top(30%)
            .bottom(30%)

        addImageView.pin
            .width(addImageView.bounds.height)
            .right()
    }

    // MARK: Actions

    @objc
    private func didTapAddImageView() {
        output?.didTapAddImageView()
    }
}
