//
//  BestViewController.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit
import PinLayout

final class BestViewController: UIViewController {
    var output: BestViewOutput?

    // MARK: UI elements

    private weak var fightsCollectionView: UICollectionView!

    // MARK: Setup

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        output?.didLoadView()
    }

    private func setupView() {
        view.backgroundColor = UIColor.supportColor
    }

    private func setupSubviews() {
        setupFightsCollectionView()
    }

    private func setupFightsCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()

        flowLayout.itemSize = CGSize(width: 90%.of(UIScreen.main.bounds.width),
                                     height: 15%.of(UIScreen.main.bounds.height))

        let collectionView = UICollectionView(frame: .null,
                                              collectionViewLayout: flowLayout)

        collectionView.backgroundColor = UIColor.supportColor
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(BestCollectionViewCell.self,
                                forCellWithReuseIdentifier: BestCollectionViewCell.cellIdentifier)
        collectionView.register(BestCollectionHeaderView.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: BestCollectionHeaderView.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self

        let refreshControl = UIRefreshControl()

        refreshControl.addTarget(self, action: #selector(didSwap), for: .valueChanged)
        collectionView.refreshControl = refreshControl

        view.addSubview(collectionView)
        fightsCollectionView = collectionView
    }

    // MARK: Layout

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutFightsCollectionView()
    }

    private func layoutFightsCollectionView() {
        fightsCollectionView.pin
            .top()
            .bottom()
            .width(90%)
            .hCenter()
    }

    // MARK: Actions

    @objc
    private func didSwap() {
        output?.didLoadView()
    }
}

extension BestViewController: UICollectionViewDelegate {

}

extension BestViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 90%.of(UIScreen.main.bounds.width),
                      height: 7%.of(UIScreen.main.bounds.height))
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90%.of(UIScreen.main.bounds.width),
                      height: 15%.of(UIScreen.main.bounds.height))
    }
}

extension BestViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: BestCollectionHeaderView.reuseIdentifier,
                                                                             for: indexPath)
            guard let header = headerView as? BestCollectionHeaderView else {
                return headerView
            }

            header.output = output

            return header
        }

        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getBestFightsCount() ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BestCollectionViewCell.cellIdentifier, for: indexPath) as? BestCollectionViewCell else {
            return UICollectionViewCell()
        }

        guard let viewModel = output?.viewModel(index: indexPath.row) else {
            return cell
        }

        cell.index = indexPath.row
        cell.output = output
        cell.configure(model: viewModel)

        return cell
    }
}

extension BestViewController: BestViewInput {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    func reloadData() {
        fightsCollectionView.refreshControl?.endRefreshing()
        fightsCollectionView.reloadData()
    }
}
