//
//  BestCollectionViewCell.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit
import PinLayout

final class BestCollectionViewCell: UICollectionViewCell {

    var output: BestViewOutput?

    var index: Int?

    static let cellIdentifier: String = "bestCell"

    // MARK: UI elements

    private weak var indexLabel: UILabel!

    private weak var voicesLabel: UILabel!

    private weak var leftFighterNameLabel: UILabel!

    private weak var leftFighterSurnameLabel: UILabel!

    private weak var separatorLabel: UILabel!

    private weak var rightFighterNameLabel: UILabel!

    private weak var rightFighterSurnameLabel: UILabel!

    // MARK: Setup

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupSubviews()

        guard let role = UserDefaultsService.shared.getUserRole(),
              role == "matchmaker" else {
            return
        }

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didDoubleTapCell))

        gestureRecognizer.numberOfTapsRequired = 2
        addGestureRecognizer(gestureRecognizer)
    }

    required init?(coder: NSCoder) {
        return nil
    }

    func configure(model: BestFightViewModel) {
        indexLabel.text = String(model.index)

        let fighterA = model.nameFighterA.split(separator: " ")
        let fighterB = model.nameFighterB.split(separator: " ")

        leftFighterNameLabel.text = String(fighterA[0])

        if fighterA.count > 1 {
            leftFighterSurnameLabel.text = String(fighterA[1])
        }

        rightFighterNameLabel.text = String(fighterB[0])

        if fighterB.count > 1 {
            rightFighterSurnameLabel.text = String(fighterB[1])
        }

        voicesLabel.text = "Voices - \(model.voices)"
    }

    private func setupView() {
        layer.cornerRadius = 15
        backgroundColor = UIColor.mainColor
    }

    private func setupSubviews() {
        setupIndexLabel()
        setupVoicesLabel()
        setupLeftFighterNameLabel()
        setupLeftFighterSurnameLabel()
        setupSeparatorLabel()
        setupRightFighterNameLabel()
        setupRightFighterSurnameLabel()
    }

    private func setupIndexLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Bold",
                                        fontSize: 20,
                                        textColor: .white,
                                        textAlignment: .left,
                                        text: String())

        addSubview(label)
        indexLabel = label
    }

    private func setupVoicesLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: String())

        addSubview(label)
        voicesLabel = label
    }

    private func setupLeftFighterNameLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 19,
                                        textColor: .white,
                                        text: String())

        addSubview(label)
        leftFighterNameLabel = label
    }

    private func setupLeftFighterSurnameLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 19,
                                        textColor: .white,
                                        text: String())

        addSubview(label)
        leftFighterSurnameLabel = label
    }

    private func setupSeparatorLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: "vs")

        addSubview(label)
        separatorLabel = label
    }

    private func setupRightFighterNameLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 19,
                                        textColor: .white,
                                        text: String())

        addSubview(label)
        rightFighterNameLabel = label
    }

    private func setupRightFighterSurnameLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 19,
                                        textColor: .white,
                                        text: String())

        addSubview(label)
        rightFighterSurnameLabel = label
    }

    public func configure(model: AnyObject) {

    }

    // MARK: Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutIndexLabel()
        layoutVoicesLabel()
        layoutLeftFighterNameLabel()
        layoutLeftFighterSurnameLabel()
        layoutSeparatorLabel()
        layoutRightFighterNameLabel()
        layoutRightFighterSurnameLabel()
    }

    private func layoutIndexLabel() {
        indexLabel.pin
            .left(5%)
            .width(25%)
            .height(25%)
            .top(5%)
    }

    private func layoutVoicesLabel() {
        voicesLabel.pin
            .width(60%)
            .hCenter()
            .height(25%)
            .top(5%)

    }

    private func layoutLeftFighterNameLabel() {
        leftFighterNameLabel.pin
            .below(of: indexLabel)
            .width(40%)
            .left(5%)
            .height(20%)
    }

    private func layoutLeftFighterSurnameLabel() {
        leftFighterSurnameLabel.pin
            .height(20%)
            .below(of: leftFighterNameLabel)
            .width(40%)
            .left(5%)
    }

    private func layoutSeparatorLabel() {
        separatorLabel.pin
            .height(20%)
            .width(10%)
            .hCenter()
            .vCenter()
    }

    private func layoutRightFighterNameLabel() {
        rightFighterNameLabel.pin
            .below(of: indexLabel)
            .width(40%)
            .right(5%)
            .height(20%)
    }

    private func layoutRightFighterSurnameLabel() {
        rightFighterSurnameLabel.pin
            .height(20%)
            .below(of: rightFighterNameLabel)
            .width(40%)
            .right(5%)
    }

    // MARK: Actions

    @objc
    private func didDoubleTapCell() {
        guard let index = index else {
            return
        }

        output?.didDoubleTapCell(index: index)
    }
}
