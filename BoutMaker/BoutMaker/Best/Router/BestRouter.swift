//
//  BestRouter.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit

final class BestRouter {
    weak var viewController: UIViewController?
}

extension BestRouter: BestRouterInput {
    func showAddBestScreen() {
        let container = AddBestContainer.assemble(with: AddBestContext())

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.navigationController?.pushViewController(container.viewController,
                                                                 animated: true)
    }
}
