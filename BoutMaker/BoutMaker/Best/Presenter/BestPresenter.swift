//
//  BestPresenter.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import Foundation

struct BestFightViewModel {
    let index: Int

    let nameFighterA: String

    let nameFighterB: String

    let voices: String

    let id: Int
}

final class BestPresenter {
    weak var view: BestViewInput?

    var router: BestRouterInput?

    private var bestFights: [BestFightViewModel]?

    init() {}
}

extension BestPresenter: BestViewOutput {
    func didDoubleTapCell(index: Int) {
        guard let fights = bestFights else {
            return
        }

        DataService.shared.deleteBestFight(id: fights[index].id) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            self?.didLoadView()
        }
    }

    func getBestFightsCount() -> Int? {
        return bestFights?.count ?? 0
    }

    func viewModel(index: Int) -> BestFightViewModel? {
        return bestFights?[index]
    }

    func didTapAddImageView() {
        router?.showAddBestScreen()
    }

    func didLoadView() {
        DataService.shared.getBestFights { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            guard let data = result.data,
                  let self = self else {
                return
            }

            var index = 0

            self.bestFights = data.map { raw in
                index += 1

                return BestFightViewModel(index: index,
                                          nameFighterA: raw.nameFighterA,
                                          nameFighterB: raw.nameFighterB,
                                          voices: raw.voices,
                                          id: raw.id)
            }

            self.view?.reloadData()
        }
    }
}
