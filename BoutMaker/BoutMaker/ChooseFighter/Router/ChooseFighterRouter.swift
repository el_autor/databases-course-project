//
//  ChooseFighterRouter.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit

final class ChooseFighterRouter {
    weak var viewController: UIViewController?
}

extension ChooseFighterRouter: ChooseFighterRouterInput {
    func goBack() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
