//
//  ChooseFighterPresenter.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import Foundation

struct FighterViewModel {
    let fullName: String

    let weightclass: String

    let record: String

    var isFavourite: Bool
}

final class ChooseFighterPresenter {
    weak var view: ChooseFighterViewInput?

    var router: ChooseFighterRouterInput?

    private var fighters: [FighterRaw]?

    private var favouriteFightersIds: Set<Int>?

    private var currentResult: [FighterViewModel]?

    private var currentResultRaw: [FighterRaw]?

    var onChoose: ((FighterRaw) -> Void)

    init(onChoose: @escaping ((FighterRaw) -> Void)) {
        self.onChoose = onChoose
    }

    private func isFavouriteFighter(fighter: FighterRaw) -> Bool {
        guard let ids = favouriteFightersIds else {
            return false
        }

        return ids.contains(fighter.id)
    }
}

extension ChooseFighterPresenter: ChooseFighterViewOutput {
    func didChooseFighter(index: IndexPath) {
        guard let fighter = currentResultRaw?[index.row] else {
            return
        }

        onChoose(fighter)
        router?.goBack()
    }

    func didTapGoBackImageView() {
        router?.goBack()
    }

    func didLoadView() {
        DataService.shared.getFighters { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            guard let data = result.data else {
                return
            }

            self?.fighters = data
        }

        DataService.shared.getFavouriteFightersForUser(userId: UserDefaultsService.shared.getUserId()) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            guard let data = result.data else {
                return
            }

            self?.favouriteFightersIds = Set(data.fighterIds)
        }
    }

    func didTapView() {
        view?.disableKeyboard()
    }

    func getUsersCount() -> Int {
        return currentResult?.count ?? 0
    }

    func didDoubleTapCell(index: Int) {
        guard let fighter = currentResultRaw?[index],
              let isFavourite = currentResult?[index].isFavourite else {
            return
        }

        if isFavourite {
            DataService.shared.deleteFanForFighter(userId: UserDefaultsService.shared.getUserId(), fighterId: fighter.id) { [weak self] result in
                guard result.error == nil else {
                    guard let networkError = result.error else {
                        return
                    }

                    switch networkError {
                    case .networkNotReachable:
                        self?.view?.showAlert(title: "Oops!", message: "No connection")
                    default:
                        self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                    }

                    return
                }

                self?.favouriteFightersIds?.remove(fighter.id)
                self?.currentResult?[index].isFavourite = !isFavourite
                self?.view?.reloadData(index: index)
            }
        } else {
            DataService.shared.saveFanForFighter(userId: UserDefaultsService.shared.getUserId(), fighterId: fighter.id, name: "\(fighter.firstName) \(fighter.lastName)") { [weak self] result in
                guard result.error == nil else {
                    guard let networkError = result.error else {
                        return
                    }

                    switch networkError {
                    case .networkNotReachable:
                        self?.view?.showAlert(title: "Oops!", message: "No connection")
                    default:
                        self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                    }

                    return
                }

                self?.favouriteFightersIds?.insert(fighter.id)
                self?.currentResult?[index].isFavourite = !isFavourite
                self?.view?.reloadData(index: index)
            }
        }
    }

    func viewModel(index: Int) -> FighterViewModel? {
        return currentResult?[index]
    }

    func didReceiveSearchString(string: String?) {
        guard let fighters = fighters,
              let string = string,
              !string.isEmpty else {
            return
        }

        DispatchQueue.global().async { [weak self] in
            self?.currentResultRaw = []
            self?.currentResult = fighters.filter { fighter in
                return fighter.lastName.lowercased().contains(string.lowercased()) || fighter.firstName.lowercased().contains(string.lowercased())
            }.map { fighterRaw in
                var winsString: String?
                var lossesString: String?
                var drawsString: String?

                if let wins = fighterRaw.wins {
                    winsString = String(wins)
                }

                if let losses = fighterRaw.losses {
                    lossesString = String(losses)
                }

                if let draws = fighterRaw.draws {
                    drawsString = String(draws)
                }

                self?.currentResultRaw?.append(fighterRaw)

                return FighterViewModel(fullName: "\(fighterRaw.firstName) \(fighterRaw.lastName)",
                                        weightclass: fighterRaw.weightclass ?? "No info",
                                        record: "\(winsString ?? "?")-\(lossesString ?? "?")-\(drawsString ?? "?")",
                                        isFavourite: self?.isFavouriteFighter(fighter: fighterRaw) ?? false)
            }

            DispatchQueue.main.async {
                if self?.currentResult != nil {
                    self?.view?.reloadData()
                }
            }
        }
    }
}
