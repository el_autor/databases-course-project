//
//  ChooseFighterCollectionViewCell.swift
//  BoutMaker
//
//  Created by Vladislav Krivozubov on 17.04.2021.
//

import UIKit
import PinLayout

final class ChooseFighterCollectionViewCell: UICollectionViewCell {

    static let cellIdentifier: String = "chooseFighterCell"

    // MARK: UI elements

    private var nameLabel: UILabel!

    private var heartImageView: UIImageView!

    private var categoryLabel: UILabel!

    private var recordLabel: UILabel!

    private var indexPath: IndexPath?

    var output: ChooseFighterViewOutput?

    // MARK: Setup

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupSubviews()
    }

    required init?(coder: NSCoder) {
        return nil
    }

    func configure(viewModel: FighterViewModel, index: IndexPath) {
        nameLabel.text = viewModel.fullName
        categoryLabel.text = viewModel.weightclass
        recordLabel.text = viewModel.record
        self.indexPath = index

        if viewModel.isFavourite {
            heartImageView.tintColor = .systemYellow
            heartImageView.isHidden = false
        } else {
            heartImageView.isHidden = true
        }
    }

    private func setupView() {
        layer.cornerRadius = 15
        backgroundColor = UIColor.mainColor

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapCell))

        gestureRecognizer.numberOfTapsRequired = 1
        addGestureRecognizer(gestureRecognizer)

        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didDoubleTapCell))

        doubleTapRecognizer.numberOfTapsRequired = 2
        addGestureRecognizer(doubleTapRecognizer)

        gestureRecognizer.require(toFail: doubleTapRecognizer)
    }

    private func setupSubviews() {
        setupNameLabel()
        setupHeartImageView()
        setupCategoryLabel()
        setupRecordLabel()
    }

    private func setupNameLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 20,
                                        textColor: .white,
                                        text: "Conor McGregor")

        addSubview(label)
        nameLabel = label
    }

    private func setupHeartImageView() {
        let imageView = UIImageView()

        imageView.contentMode = .scaleAspectFit

        imageView.image = UIImage(systemName: "star.fill")
        imageView.tintColor = .systemYellow
        imageView.isHidden = true

        heartImageView = imageView
        addSubview(heartImageView)
    }

    private func setupCategoryLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: "Lightweight")

        addSubview(label)
        categoryLabel = label
    }

    private func setupRecordLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 15,
                                        textColor: .white,
                                        text: "24-5-1")

        addSubview(label)
        recordLabel = label
    }

    // MARK: Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutNameLabel()
        layoutHeartImageView()
        layoutCategoryLabel()
        layoutRecordLabel()
    }

    private func layoutNameLabel() {
        nameLabel.pin
            .width(60%)
            .height(40%)
            .top(10%)
            .hCenter()
    }

    private func layoutHeartImageView() {
        heartImageView.pin
            .width(10%)

        heartImageView.pin
            .height(heartImageView.bounds.width)
            .top(15%)
            .right(5%)
    }

    private func layoutCategoryLabel() {
        categoryLabel.pin
            .below(of: nameLabel)
            .width(50%)
            .height(40%)
            .left()
    }

    private func layoutRecordLabel() {
        recordLabel.pin
            .below(of: nameLabel)
            .width(50%)
            .height(40%)
            .right()
    }

    // MARK: Actions

    @objc
    private func didTapCell() {
        guard let index = indexPath else {
            return
        }

        output?.didChooseFighter(index: index)
    }

    @objc
    private func didDoubleTapCell() {
        guard let index = indexPath?.row else {
            return
        }

        output?.didDoubleTapCell(index: index)
    }
}
