//
//  ChooseFighterCollectionViewHeader.swift
//  BoutMaker
//
//  Created by Vladislav Krivozubov on 17.04.2021.
//

import UIKit
import PinLayout

final class ChooseFighterCollectionViewHeader: UICollectionReusableView {
    static let reuseIdentifier = "chooseFighterHeader"

    weak var output: ChooseFighterViewOutput?

    // MARK: UI Elements

    private var titleLabel: UILabel!

    private var goBackImageView: UIImageView!

    var searchTextField: UITextField!

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupSubviews()
    }

    required init?(coder: NSCoder) {
        return nil
    }

    // MARK: Setup

    private func setupView() {

    }

    private func setupSubviews() {
        setupTitleLabel()
        setupGoBackImageView()
        setupSearchTextField()
    }

    private func setupTitleLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 25,
                                        textColor: .white,
                                        text: "Fighters")

        addSubview(label)
        titleLabel = label
    }

    private func setupGoBackImageView() {
        let imageView = UIImageView()

        imageView.image = UIImage(systemName: "chevron.backward",
                                  withConfiguration: UIImage.SymbolConfiguration(weight: .bold))
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFit

        let tapRecognizer = UITapGestureRecognizer(target: self,
                                                   action: #selector(didTapGoBackImageView))
        tapRecognizer.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tapRecognizer)
        imageView.isUserInteractionEnabled = true

        addSubview(imageView)
        goBackImageView = imageView
    }

    private func setupSearchTextField() {
        searchTextField = UITextField.customTextField(placeholder: "Search",
                                                      fontsize: 15,
                                                      fontColor: .white,
                                                      backgroundColor: .gray)

        searchTextField.delegate = self

        addSubview(searchTextField)
    }

    // MARK: Layout

    override func layoutSubviews() {
        super.layoutSubviews()

        layoutTitleLabel()
        layoutGoBackImageView()
        layoutSearchTextField()
    }

    private func layoutTitleLabel() {
        titleLabel.pin
            .top(10%)
            .width(70%)
            .height(40%)
            .hCenter()
    }

    private func layoutGoBackImageView() {
        goBackImageView.pin
            .top(18%)
            .height(24%)

        goBackImageView.pin
            .width(goBackImageView.bounds.height)
            .left()
    }

    private func layoutSearchTextField() {
        searchTextField.pin
            .below(of: titleLabel)
            .marginTop(5%)
            .width(100%)
            .height(30%)
            .hCenter()
    }

    // MARK: Actions

    @objc
    private func didTapGoBackImageView() {
        output?.didTapGoBackImageView()
    }
}

extension ChooseFighterCollectionViewHeader: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        guard let text = textField.text else {
            return true
        }

        output?.didReceiveSearchString(string: NSString(string: text).replacingCharacters(in: range, with: string))
        return true
    }

}
