//
//  ChooseFighterViewController.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit
import PinLayout

final class ChooseFighterViewController: UIViewController {
    var output: ChooseFighterViewOutput?

    // MARK: UI Elements

    private var fightersCollectionView: UICollectionView!

    private var headerView: ChooseFighterCollectionViewHeader?

    // MARK: Setup

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()

        output?.didLoadView()
    }

    private func setupView() {
        view.backgroundColor = UIColor.supportColor

        let recognizer = UITapGestureRecognizer(target: self,
                                                action: #selector(didTapView))

        recognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(recognizer)
    }

    private func setupSubviews() {
        setupFightersCollectionView()
    }

    private func setupFightersCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()

        flowLayout.itemSize = CGSize(width: 90%.of(UIScreen.main.bounds.width),
                                     height: 15%.of(UIScreen.main.bounds.height))

        let collectionView = UICollectionView(frame: .null,
                                              collectionViewLayout: flowLayout)

        collectionView.isUserInteractionEnabled = true
        collectionView.backgroundColor = UIColor.supportColor
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(ChooseFighterCollectionViewCell.self,
                                forCellWithReuseIdentifier: ChooseFighterCollectionViewCell.cellIdentifier)
        collectionView.register(ChooseFighterCollectionViewHeader.self,
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: ChooseFighterCollectionViewHeader.reuseIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self

        view.addSubview(collectionView)
        fightersCollectionView = collectionView
    }

    // MARK: Layout

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutFightersCollectionView()
    }

    private func layoutFightersCollectionView() {
        fightersCollectionView.pin
            .top()
            .bottom()
            .width(90%)
            .hCenter()
    }

    // MARK: Actions

    @objc
    private func didTapView() {
        output?.didTapView()
    }
}

extension ChooseFighterViewController: UICollectionViewDelegate {}

extension ChooseFighterViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 90%.of(UIScreen.main.bounds.width),
                      height: 12%.of(UIScreen.main.bounds.height))
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90%.of(UIScreen.main.bounds.width),
                      height: 12%.of(UIScreen.main.bounds.height))
    }
}

extension ChooseFighterViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: ChooseFighterCollectionViewHeader.reuseIdentifier,
                                                                             for: indexPath)
            guard let header = headerView as? ChooseFighterCollectionViewHeader else {
                return headerView
            }

            self.headerView = header
            header.output = output

            return header
        }

        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getUsersCount() ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ChooseFighterCollectionViewCell.cellIdentifier, for: indexPath) as? ChooseFighterCollectionViewCell else {
            return UICollectionViewCell()
        }

        guard let viewModel = output?.viewModel(index: indexPath.row) else {
            return cell
        }

        cell.output = output
        cell.configure(viewModel: viewModel, index: indexPath)

        return cell
    }
}

extension ChooseFighterViewController: ChooseFighterViewInput {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }

    func disableKeyboard() {
        view.endEditing(false)
    }

    func reloadData() {
        fightersCollectionView.performBatchUpdates({
            let deleteIndexPaths = Array(0..<fightersCollectionView.numberOfItems(inSection: 0)).map { IndexPath(item: $0, section: 0) }
            fightersCollectionView.deleteItems(at: deleteIndexPaths)

            let insertIndexPaths = Array(0..<(output?.getUsersCount() ?? 0)).map { IndexPath(item: $0, section: 0) }
            fightersCollectionView.insertItems(at: insertIndexPaths)
        },
                                                   completion: nil)
        headerView?.searchTextField.becomeFirstResponder()
    }

    func reloadData(index: Int?) {
        if let index = index {
            UIView.performWithoutAnimation {
                fightersCollectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
            }
        } else {
            fightersCollectionView.reloadData()
        }
    }
}
