//
//  ChooseFighterContainer.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import UIKit

final class ChooseFighterContainer {
    let viewController: UIViewController

    static func assemble(with context: ChooseFighterContext) -> ChooseFighterContainer {
        let viewController = ChooseFighterViewController()
        let presenter = ChooseFighterPresenter(onChoose: context.onChoose)
        let router = ChooseFighterRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return ChooseFighterContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct ChooseFighterContext {
    let onChoose: ((FighterRaw) -> Void)
}
