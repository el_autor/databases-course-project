//
//  ChooseFighterProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 04.04.2021.
//

import Foundation

protocol ChooseFighterViewInput: AnyObject {
    func showAlert(title: String, message: String)

    func reloadData()

    func reloadData(index: Int?)

    func disableKeyboard()
}

protocol ChooseFighterViewOutput: AnyObject {
    func didTapGoBackImageView()

    func didLoadView()

    func didTapView()

    func didDoubleTapCell(index: Int)

    func didReceiveSearchString(string: String?)

    func getUsersCount() -> Int

    func viewModel(index: Int) -> FighterViewModel?

    func didChooseFighter(index: IndexPath)
}

protocol ChooseFighterRouterInput: AnyObject {
    func goBack()
}
