//
//  MainTabBarViewController.swift
//  BoutMaker
//
//  Created by Vlad on 25.03.2021.
//

import UIKit

final class MainTabBar: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
    }

    private func setupView() {
        tabBar.barTintColor = UIColor.mainColor
        tabBar.tintColor = .black
        tabBar.unselectedItemTintColor = .white
    }
}
