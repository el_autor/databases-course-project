//
//  MainTabBarContainer.swift
//  BoutMaker
//
//  Created by Vlad on 25.03.2021.
//

import UIKit

final class MainTabBarContainer {
    let viewController: UIViewController

    static func assemble() -> MainTabBarContainer {
        let tabBarVC = MainTabBar()

        tabBarVC.viewControllers = [prepareEventsScreen(),
                                    prepareBestScreen(),
                                    prepareRatingScreen(),
                                    prepareProfileScreen()]

        return MainTabBarContainer(viewController: tabBarVC)
    }

    private static func prepareEventsScreen() -> UINavigationController {
        let container = EventsContainer.assemble(with: EventsContext())
        let tabBarItem = UITabBarItem(title: "Events",
                                      image: UIImage(named: "fightIcon"),
                                      tag: 0)

        container.viewController.modalPresentationStyle = .fullScreen
        container.viewController.tabBarItem = tabBarItem

        let navigationVC = UINavigationController(rootViewController: container.viewController)

        navigationVC.navigationBar.isHidden = true

        return navigationVC
    }

    private static func prepareBestScreen() -> UINavigationController {
        let container = BestContainer.assemble(with: BestContext())
        let tabBarItem = UITabBarItem(title: "Best",
                                      image: UIImage(named: "fireIcon"),
                                      tag: 1)

        container.viewController.modalPresentationStyle = .fullScreen
        container.viewController.tabBarItem = tabBarItem

        let navigationVC = UINavigationController(rootViewController: container.viewController)

        navigationVC.navigationBar.isHidden = true

        return navigationVC
    }

    private static func prepareRatingScreen() -> UINavigationController {
        let container = RatingContainer.assemble(with: RatingContext())
        let tabBarItem = UITabBarItem(title: "Rating",
                                      image: UIImage(named: "leaderboardIcon"),
                                      tag: 2)

        container.viewController.modalPresentationStyle = .fullScreen
        container.viewController.tabBarItem = tabBarItem

        let navigationVC = UINavigationController(rootViewController: container.viewController)

        navigationVC.navigationBar.isHidden = true

        return navigationVC
    }

    private static func prepareProfileScreen() -> UINavigationController {
        let container = ProfileContainer.assemble(with: ProfileContext())
        let tabBarItem = UITabBarItem(title: "Profile",
                                      image: UIImage(named: "profileIcon"),
                                      tag: 3)

        container.viewController.modalPresentationStyle = .fullScreen
        container.viewController.tabBarItem = tabBarItem

        let navigationVC = UINavigationController(rootViewController: container.viewController)

        navigationVC.navigationBar.isHidden = true

        return navigationVC
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}
