//
//  UnderscreenContainer.swift
//  BoutMaker
//
//  Created by Vlad on 16.03.2021.
//

import UIKit

final class UnderscreenContainer {
    let viewController: UIViewController

    static func assemble(with context: UnderscreenContext) -> UnderscreenContainer {
        let viewController = UnderscreenViewController()
        let presenter = UnderscreenPresenter()
        let router = UnderscreenRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return UnderscreenContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct UnderscreenContext { }
