//
//  UnderscreenRouter.swift
//  BoutMaker
//
//  Created by Vlad on 16.03.2021.
//

import UIKit

final class UnderscreenRouter {
    weak var viewController: UIViewController?
}

extension UnderscreenRouter: UnderscreenRouterInput {
    func showLoginScreen() {
        let container = LoginContainer.assemble(with: LoginContext())

        container.viewController.modalPresentationStyle = .fullScreen

        let navigationVC = UINavigationController(rootViewController: container.viewController)

        navigationVC.isNavigationBarHidden = true
        navigationVC.modalPresentationStyle = .fullScreen

        viewController?.present(navigationVC,
                                animated: false,
                                completion: nil)
    }

    func showMainTabBar() {
        let container = MainTabBarContainer.assemble()

        container.viewController.modalPresentationStyle = .fullScreen

        let navigationVC = UINavigationController(rootViewController: container.viewController)

        navigationVC.isNavigationBarHidden = true
        navigationVC.modalPresentationStyle = .fullScreen

        viewController?.present(navigationVC,
                                animated: true,
                                completion: nil)
    }

    func showChooseRoleScreen(username: String) {
        let container = ChooseRoleContainer.assemble(with: ChooseRoleContext(username: username))

        container.viewController.modalPresentationStyle = .fullScreen

        viewController?.present(container.viewController,
                                animated: true,
                                completion: nil)
    }
}
