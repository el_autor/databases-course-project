//
//  UnderscreenProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 16.03.2021.
//

protocol UnderscreenViewInput: AnyObject {
    func showAlert(title: String, message: String)
}

protocol UnderscreenViewOutput: AnyObject {
    func didFinishAnimations()
}

protocol UnderscreenRouterInput: AnyObject {
    func showLoginScreen()

    func showMainTabBar()

    func showChooseRoleScreen(username: String)
}
