import UIKit
import PinLayout

final class UnderscreenViewController: UIViewController {

    var output: UnderscreenViewOutput?

    // MARK: UI elements

    private weak var octagonImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutOctagonImageView()
    }

    private func setupView() {
        view.backgroundColor = UIColor.mainColor
    }

    private func setupSubviews() {
        setupOctagonImageView()
    }

    private func setupOctagonImageView() {
        let imageView = UIImageView()

        octagonImageView = imageView
        view.addSubview(octagonImageView)

        octagonImageView.contentMode = .scaleToFill
        octagonImageView.image = UIImage(systemName: "octagon")
        octagonImageView.tintColor = .black
    }

    private func layoutOctagonImageView() {
        octagonImageView.pin
            .hCenter()
            .vCenter()
            .width(57%)
            .height(23%)

        UIView.animateKeyframes(withDuration: 1.5,
                                delay: 0,
                                options: [],
                                animations: { [weak self] in
                                    UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.3) {
                                        ()
                                    }

                                    UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7) { [weak self] in
                                        self?.octagonImageView.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
                                    }

                                    UIView.addKeyframe(withRelativeStartTime: 0.7, relativeDuration: 0.3) { [weak self] in
                                        self?.octagonImageView.transform = CGAffineTransform.identity.scaledBy(x: 10, y: 10)
                                    }
                                },
                                completion: { [weak self] _ in
                                    self?.output?.didFinishAnimations()
                                })
    }
}

extension UnderscreenViewController: UnderscreenViewInput {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }
}
