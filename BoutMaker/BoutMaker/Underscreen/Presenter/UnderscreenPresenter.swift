//
//  UnderscreenPresenter.swift
//  BoutMaker
//
//  Created by Vlad on 16.03.2021.
//

final class UnderscreenPresenter {
    weak var view: UnderscreenViewInput?

    var router: UnderscreenRouterInput?
}

extension UnderscreenPresenter: UnderscreenViewOutput {
    func didFinishAnimations() {
        if UserDefaultsService.shared.isAuthorized() {
            guard let username = UserDefaultsService.shared.getUsername(),
                  let password = UserDefaultsService.shared.getPassword() else {
                return
            }

            AuthService.shared.login(username: username,
                                     password: password) { [weak self] result in
                guard result.error == nil else {
                    guard let networkError = result.error else {
                        return
                    }

                    switch networkError {
                    case .networkNotReachable:
                        self?.view?.showAlert(title: "Oops!", message: "No connection")
                    case .userNotExist:
                        self?.view?.showAlert(title: "Oops!", message: "User not exist")
                    default:
                        self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                    }

                    return
                }

                guard let data = result.data else {
                    return
                }

                if data.role == "default" {
                    guard let username = UserDefaultsService.shared.getUsername() else {
                        return
                    }

                    self?.router?.showChooseRoleScreen(username: username)
                } else {
                    self?.router?.showMainTabBar()
                }
            }
        } else {
            router?.showLoginScreen()
        }
    }
}
