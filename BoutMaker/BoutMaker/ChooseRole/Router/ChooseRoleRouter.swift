//
//  ChooseRoleRouter.swift
//  BoutMaker
//
//  Created by Vlad on 25.03.2021.
//

import UIKit

final class ChooseRoleRouter {
    weak var viewController: UIViewController?
}

extension ChooseRoleRouter: ChooseRoleRouterInput {
    func showMainTabBar() {
        let container = MainTabBarContainer.assemble()

        container.viewController.modalPresentationStyle = .fullScreen

        if viewController?.navigationController != nil {
            viewController?.navigationController?.pushViewController(container.viewController,
                                                                 animated: true)
        } else {
            viewController?.present(container.viewController,
                                    animated: true,
                                    completion: nil)
        }
    }
}
