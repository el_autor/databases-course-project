//
//  ChooseRoleProtocols.swift
//  BoutMaker
//
//  Created by Vlad on 25.03.2021.
//

protocol ChooseRoleViewInput: AnyObject {
    func showAlert(title: String, message: String)
}

protocol ChooseRoleViewOutput: AnyObject {
    func didTapFanButton()

    func didTapMatchmakerButton()
}

protocol ChooseRoleRouterInput: AnyObject {
    func showMainTabBar()
}
