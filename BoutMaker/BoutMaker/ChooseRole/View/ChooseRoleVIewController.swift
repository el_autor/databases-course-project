//
//  ChooseRoleVIewController.swift
//  BoutMaker
//
//  Created by Vlad on 25.03.2021.
//

import UIKit
import PinLayout

final class ChooseRoleViewController: UIViewController {
    var output: ChooseRoleViewOutput?

    private weak var questionLabel: UILabel!

    private weak var fanButton: UIButton!

    private weak var matchmakerButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupSubviews()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        layoutQuestionLabel()
        layoutFanButton()
        layoutMatchmakerButton()
    }

    private func setupView() {
        view.backgroundColor = UIColor.supportColor
    }

    private func setupSubviews() {
        setupQuestionLabel()
        setupFanButton()
        setupMatchmakerButton()
    }

    private func setupQuestionLabel() {
        let label = UILabel.customLabel(fontName: "DMSans-Medium",
                                        fontSize: 30,
                                        textColor: .white,
                                        text: "Who are you?")

        view.addSubview(label)
        questionLabel = label
    }

    private func setupFanButton() {
        let button = UIButton.customButton(title: "I am a fan",
                                           backgroundColor: UIColor.mainColor,
                                           cornerRadius: 10,
                                           fontName: "DMSans-Bold",
                                           fontSize: 16,
                                           fontColor: .white)

        view.addSubview(button)
        fanButton = button

        fanButton.addTarget(self,
                            action: #selector(didTapFanButton),
                            for: .touchUpInside)
    }

    private func setupMatchmakerButton() {
        let button = UIButton.customButton(title: "I am a matchmaker",
                                           backgroundColor: UIColor.mainColor,
                                           cornerRadius: 10,
                                           fontName: "DMSans-Bold",
                                           fontSize: 16,
                                           fontColor: .white)

        view.addSubview(button)
        matchmakerButton = button

        matchmakerButton.addTarget(self,
                            action: #selector(didTapMatchmakerButton),
                            for: .touchUpInside)
    }

    // MARK: Layout

    private func layoutQuestionLabel() {
        questionLabel.pin
            .top(35%)
            .hCenter()
            .width(100%)
            .height(5%)
    }

    private func layoutFanButton() {
        fanButton.pin
            .below(of: questionLabel)
            .marginTop(5%)
            .width(90%)
            .hCenter()
            .height(6%)
    }

    private func layoutMatchmakerButton() {
        matchmakerButton.pin
            .below(of: fanButton)
            .marginTop(2%)
            .width(90%)
            .hCenter()
            .height(6%)
    }

    @objc
    private func didTapFanButton() {
        output?.didTapFanButton()
    }

    @objc
    private func didTapMatchmakerButton() {
        output?.didTapMatchmakerButton()
    }
}

extension ChooseRoleViewController: ChooseRoleViewInput {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ОК", style: .default, handler: nil)

        alert.addAction(okAction)

        present(alert, animated: true, completion: nil)
    }
}
