//
//  ChooseRoleContainer.swift
//  BoutMaker
//
//  Created by Vlad on 25.03.2021.
//

import UIKit

final class ChooseRoleContainer {
    let viewController: UIViewController

    static func assemble(with context: ChooseRoleContext) -> ChooseRoleContainer {
        let viewController = ChooseRoleViewController()
        let presenter = ChooseRolePresenter(username: context.username)
        let router = ChooseRoleRouter()

        viewController.output = presenter
        presenter.view = viewController
        presenter.router = router
        router.viewController = viewController

        return ChooseRoleContainer(viewController: viewController)
    }

    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
}

struct ChooseRoleContext {
    var username: String
}
