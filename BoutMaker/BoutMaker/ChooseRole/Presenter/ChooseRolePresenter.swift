//
//  ChooseRolePresenter.swift
//  BoutMaker
//
//  Created by Vlad on 25.03.2021.
//

final class ChooseRolePresenter {
    private struct Model {
        var username: String
    }

    private var model: Model

    weak var view: ChooseRoleViewInput?

    var router: ChooseRoleRouterInput?

    init(username: String) {
        model = Model(username: username)
    }
}

extension ChooseRolePresenter: ChooseRoleViewOutput {
    func didTapFanButton() {
        AuthService.shared.setRole(username: model.username,
                                   role: UserRole.fan.rawValue) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            UserDefaultsService.shared.setUserRole(role: UserRole.fan.rawValue)
            self?.router?.showMainTabBar()
        }
    }

    func didTapMatchmakerButton() {
        AuthService.shared.setRole(username: model.username,
                                   role: UserRole.matchmaker.rawValue) { [weak self] result in
            guard result.error == nil else {
                guard let networkError = result.error else {
                    return
                }

                switch networkError {
                case .networkNotReachable:
                    self?.view?.showAlert(title: "Oops!", message: "No connection")
                default:
                    self?.view?.showAlert(title: "Oops!", message: "We are working to fix the problem")
                }

                return
            }

            UserDefaultsService.shared.setUserRole(role: UserRole.matchmaker.rawValue)
            self?.router?.showMainTabBar()
        }
    }
}

enum UserRole: String {
    case fan
    case matchmaker
}
